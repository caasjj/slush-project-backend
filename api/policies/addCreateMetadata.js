'use strict';


/**
 * Policy to set 'userCreated' and 'userUpdated' to the requester id on create,
 * and only 'userUpdated' on update.  Also prevents the request from overwriting
 * these fields and the id.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.addDataCreate() called]' );

    if ( request.options.action !== 'update' )
        {
            request.body && (request.body.createdUser = request.token || -1);
            request.body && (request.body.updatedUser = request.token || -1);
        }
    else
        {
            delete request.body.createdUser;
            request.body && (request.body.updatedUser = request.token || -1);
        }
    next();

};
