'use strict';

/**
 * Policy for Sails that initializes Passport.js to use.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 */
var passport = require( 'passport' );

module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.Passport() called]' );

    request.body && ( request.body.createdUser = request.body.createdUser || -1);
    request.body && ( request.body.updatedUser = request.body.updatedUser || -1);

    // Initialize Passport
    passport.initialize()(request, response, next);

};
