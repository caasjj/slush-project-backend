'use strict';

/**
 * Policy to filter only to those model instances readable by current user
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function (request, response, next) {
    sails.log.verbose(__filename + ':' + __line + ' [Policy.isAdmin() called]');

    var acl = sails.services.acl;
    var model = request.options.model;

    if (request.query.where) {
        request.query.where = JSON.parse(request.query.where);
    }

    acl.whatReadableItems(request.token, model, function (err, idArray) {
        if (err) return next(err);

        // if request a single id via ?id=:id, just return that if it's allowed
        if (request.query.id) {
            if (idArray.indexOf(request.query.id) < 0) {
                return response.forbidden('Id ' + request.query.id + ' is not authorized.');
            }
        }
        else {

            // if requesting an array of id's via where={"id":[:id1, :id2,..]}, return forbidden if any
            // id in array is unallowed
            if (request.query.where && request.query.where.id) {

                request.query.where.id = _.isArray(request.query.where.id) ? request.query.where.id : [request.query.where.id];
                if (_.any(request.query.where.id, function (id) {
                    return (idArray.indexOf(id) < 0 && idArray.indexOf('' + id) < 0);
                })) {
                    return response.forbidden('All Id\'s are not authorized.');
                }

            }

            // otherwise, return all allowed id's filtered by the (non-id) where clause
            else {
                request.query.where = _.extend(request.query.where || {}, {"id": idArray });
            }
        }
        next();
    });

};
