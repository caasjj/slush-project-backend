'use strict';

/**
 * Policy to set the 'user' field of a 'create' request to the requester id so that a resource
 * created without association (i.e. POST /profile) will be associated to the user.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function(request, response, next) {
    sails.log.verbose(__filename + ':' + __line + ' [Policy.addDataCreate() called]');

    request.body && (request.body.user = request.token);

    next();
};
