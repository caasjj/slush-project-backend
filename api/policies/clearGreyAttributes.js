'use strict';

/**
 * Policy to remove attributes marked as 'grey' from a POST/PU, i.e. those attributes marked by the model schema as
 * not directly writable but rather only writable through domain logic.  'admin' users bypass this and can write
 * to models at will.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.isAdmin() called]' );

    var model = sails.services['urlparser'].association( request ) ||
        sails.services['urlparser'].model( request );

    if ( !request.token )
        {
            sails.models[model].clearGreyAttributes( request.body );
            sails.models[model].clearGreyAttributes( request.query );
            next()
        }
    else
        {

            var acl = sails.services.acl;

            acl.isAdmin( request['token'], function ( err, isAdmin ) {
                if ( err ) return next( err );
                if ( isAdmin ) return next();

                sails.models[model].clearGreyAttributes( request.body );
                sails.models[model].clearGreyAttributes( request.query );

                next();
            } );

        }
};
