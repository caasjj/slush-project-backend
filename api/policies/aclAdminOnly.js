'use strict';

/**
 * Policy to block access if requester does not have admin role
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.isAdmin() called]' );

    var acl = sails.services.acl;

    acl.isAdmin(request.token, function(err, isAdmin){
        if (err) return next(err);
        if (!isAdmin) return response.forbidden('Admin Only Area');
        next();
    });

};
