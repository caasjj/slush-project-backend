'use strict';

/**
 * Policy to check that request is done via a user that has ownership of / association with the resource being
 * affected:
 *  1. If the resource is a user model (/user/:id), then the 'id' MUST be equal to request.token
 *  2. For any other resource, then Resource.findOne().populate('user') MUST return a resource model populated
 *       with a user object or array with one user having an id equal to request.token
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */

module.exports = function ( request, response, next ) {

    sails.log.verbose( __filename + ':' + __line + ' [Policy.filterByUserCreated() called]' );

    if ( request.isAdmin )
        {
            return next();
        }

    if ( request.token )
        {
            request.query && (request.query.createdUser = request.token);
            next();
        }
    else
        {
            var error = new Error();

            error.message = 'Request token not present.';
            error.status = 403;

            next( error );
        }

};

