'use strict';

/**
 * Policy to check if current user is admin or not.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.isAdmin() called]' );

    var acl = sails.services.acl;

//    if ( !request.param( 'adminBypass' ) ) return next();
//
//    request.query && delete request.query.adminBypass;
//    request.body && delete request.body.adminBypass;

    // Fetch current user by the token
//    sails.models['user']
//        .findOne( request.token )
//        .exec( function exec ( error, user ) {
//            if ( error )
//                {
//                    next( error );
//                }
//            else {
//                request.isAdmin = !!(user && user.admin);
//                next();
//            }
//        } );


    acl.isAdmin(request.token, function(err, isAdmin){
        if (err) return next(err);
        if (!isAdmin) return response.forbiddeen('Admin Only Area');
        next();
    })
};
