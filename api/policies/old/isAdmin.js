'use strict';

/**
 * Policy to check if current user is admin or not.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function(request, response, next) {
    sails.log.verbose(__filename + ':' + __line + ' [Policy.isAdmin() called]');

    if (!request.isAdmin) {
        return response.forbidden('Admin only area.');
    }
    next();
};
