'use strict';

/**
 * Policy to check that request is done via a user that has ownership of / association with the resource being
 * affected:
 *  1. If the resource is a user model (/user/:id), then the 'id' MUST be equal to request.token
 *  2. For any other resource, then Resource.findOne().populate('user') MUST return a resource model populated
 *       with a user object or array with one user having an id equal to request.token
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */

module.exports = function ( request, response, next ) {

    var urlParser = sails.services.urlparser;

    sails.log.verbose( __filename + ':' + __line + ' [Policy.filterByUserCreated() called]' );

    // admin bypass
    if ( request.isAdmin )
        {
            return next();
        }

    // grab the association from /model/:id/association/:associationId
    var association = urlParser.association( request );
    var id = urlParser.associationId( request );

    // reject bogus associations
    if ( !sails.models[association] )
        {
            return response.badRequest( 'No such model to associate with' );
        }

    // associating to a new model is fine
    if ( !id ) return next();

    // make sure user owns this resource before associating
    sails
        .models[association]
        .findOne( id )
        .exec( function ( error, model ) {
            if ( error )
                {
                    next( error );
                }
            else if (!model)
            {
                response.badRequest('Cannot find instance of model to associate with');
            }
            else if ( model.createdUser !== +request.token )
                {
                    return response.forbidden( 'Not authorized to associate to this model' );
                }
            next();
        } );
};

