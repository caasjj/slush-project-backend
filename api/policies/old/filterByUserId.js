'use strict';

/**
 * Policy to check that request is done via a user that has ownership of / association with the resource being
 * affected:
 *  1. If the resource is a user model (/user/:id), then the 'id' MUST be equal to request.token
 *  2. For any other resource, then Resource.findOne().populate('user') MUST return a resource model populated
 *       with a user object or array with one user having an id equal to request.token
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */

module.exports = function ( request, response, next ) {

    var urlParser = sails.services.urlparser;

    sails.log.verbose( __filename + ':' + __line + ' [Policy.setIdToTOken() called]' );

    if ( request.isAdmin )
        {
            return next();
        }

    var model = urlParser.model( request );


    if ( model !== 'user' )
        {
            return response.badRequest( 'Policy applied to wrong action' );
        }
    request.query && (request.query.id = request.token);
    next();

};
