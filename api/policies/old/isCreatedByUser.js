'use strict';

/**
 * Policy to check that request is done via a user that has ownership of / association with the resource being
 * affected:
 *  1. If the resource is a user model (/user/:id), then the 'id' MUST be equal to request.token
 *  2. For any other resource, then Resource.findOne().populate('user') MUST return a resource model populated
 *       with a user object or array with one user having an id equal to request.token
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {

    var urlParser = sails.services.urlparser;

    if (request.isAdmin) {
        return next()
    }

    var id = urlParser.modelId(request),
        model = request.options.model || request.options.controller;

    if (!id) return next();

    sails

        .models[model]

        .findOne(id, function(err, instance) {

           if (instance.createdUser !== +request.token) {
                 return response.forbidden('Not authorized to access this resource');
           }

            next();
        });

};
