'use strict';

/***
 * Policy to make sure a POST to the webhook controller comes from the mail provider and
 * is not spoofed by a third party.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {
    var emailService = sails.services['email'];

    var message = request.params.all();
    emailService.verifyWebHook( message, function ( err ) {
        if ( err ) return response.badRequest( err );
        next();
    } )

};