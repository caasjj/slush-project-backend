'use strict';

/**
 * Policy to upload a file to the appropriate directory and give the file a suitable name based on the
 * user who uploaded it.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 */
module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.addDataCreate() called]' );

    request['file']('photofile')

        .upload(function(err, files) {

            if (err) return response.serverError(err);

            request.body.url = '/path/to/photo';
            sails.log.info('Created: ', files);

            next();

        })

};
