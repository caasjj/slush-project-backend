'use strict';

/**
 * aclCanAssociateResources - given /model/:id/assoc/:aid, blocks permission if requester does not have 'write'
 * permission on /model/:id, OR if both /assoc/:aid exists AND requester does not have 'write' access to it.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.isAdmin() called]' );

    var acl = sails.services.acl;
    var parser = sails.services.urlparser;

    var model = parser.model( request ),
        modelId = parser.modelId( request ),

        association = parser.association( request ),
        associationId = parser.associationId( request );

    acl.isAllowed( request.token, model, modelId, 'write', function ( err, ok ) {
        if ( err ) return next( err );
        if ( !ok ) return response.forbidden( 'Not allowed to update this record\'s associations' );

        if (!associationId) return next();

        acl.isAllowed( request.token, association, associationId,  'write', function(err, ok) {
            if ( err ) return next( err );
            if ( !ok ) return response.forbidden( 'Not allowed to associate to this record' );
            next();
        })
    } );

};
