'use strict';

/**
 * Policy to check if requester has 'read' permission on /model/:id.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {
    sails.log.verbose( __filename + ':' + __line + ' [Policy.isAdmin() called]' );

    var acl = sails.services.acl;
    var parser = sails.services.urlparser;

    var model = parser.model(request);
    var id = parser.modelId(request);

    acl.isAllowed(request.token, model, id, 'read', function(err, ok){
        if (err) return next(err);
        if (!ok) return response.forbidden('Not allowed to view this record');
        next();
    });

};
