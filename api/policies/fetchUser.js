'use strict';

var _ = require( 'lodash' );

/**
 * Policy to check that request is done via authenticated user. This policy uses existing
 * JWT tokens to validate that user is authenticated. If use is not authenticate policy
 * sends 401 response back to client.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 *
 * @returns {*}
 */
module.exports = function ( request, response, next ) {

    var where = {
        username:request.param( 'identifier' ) || request.param( 'username' )
    };

    sails

        .models['user']

        .findOne( where )

        .populate( 'passports')

        .exec( function ( err, user ) {

            if ( err ) return next( err );
            if ( !user ) return next( 'Invalid username' );

            var passport = _.find( user.passports, function ( passport ) {

                return passport.protocol === "local";

            } );

            passport

                .validatePassword( request.body.password, function ( err, matched ) {

                    if ( err || !matched )
                        {
                            response.unauthorized( 'Invalid Username or Password');
                        }
                    else
                        {
                            request.user = user;
                           next();
                        }
                } );


        } );

};
