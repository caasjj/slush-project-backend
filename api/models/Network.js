'use strict';

/**
 * Network.js
 *
 * @module :: Network
 *
 *
 * @description :: Waterline model of a Network object, representing a two way connection
 * between two users with additional attributes attached to the connection.
 *
 * Associations:
 *
 * User   : ManyToMany - a connection can belong to up to two users, and each user may have many connections
 */

var networkTypes = sails.config.enums.network.types;
var connectionStatusTypes = sails.config.enums.network.status;

var base = _.cloneDeep( require( '../base/Model' ) );

var model = {

    /*********************************************************
     *
     * Instance Attributes
     *
     *********************************************************/
    //******************* REQUIRED ***********************//
    attributes:{

        /**
         * @param {integer} user1
         * @description id of user receiving the invite, and who may eventually
         * accept or reject the invite
         */
        user1:{
            type    :'integer',
            required:true
        },

        /**
         * @param {integer} user2
         * @description id of user receiving the invite, and who may eventually
         * accept or reject the invite
         */
        user2 :{
            type    :'integer',
            required:true
        },
        //******************** DEFAULT ***********************//
        /**
         * @param {string} type
         * @description indicates the nature of the network connection between the
         * two users, user1 and user2
         */
        type  :{
            type      :'string',
            in        :networkTypes,
            defaultsTo:'Acquaintance'
        },
        /**
         * @param {string} status
         * @description indicates current status of the connection
         */
        status:{
            type      :'string',
            in        :connectionStatusTypes,
            defaultsTo:'Invite'
        },

        /*********************************************************
         *
         * Associations
         *
         *********************************************************/

        /**
         * @param {object} user
         * @description foreign key to join table with 'User' model in ManyToMany association
         * the key is named 'network', so this end is called 'user_network' (model_key).
         * <br>
         * Other end is 'network_user', so the join table is 'network_user__user_network'.
         * @see  {@link User}
         */
        users:{
            collection:'user',
            via       :'networks'
        }
    },

    canAssociate:function ( id, userId, callback ) {

        sails
            .models['network']
            .findOne( id )
            .exec( function ( err, instance ) {
                if (err) return callback(err);
                if (!instance) return callback( new Error('No such network instance') );
                if (userId === instance.createdUser || userId === instance.updatedUser) return callback(null, false);
                callback(null, userId === instance.user2 || userId === instance.user1);
            } );
    }

};

module.exports = _.merge( base, model );
