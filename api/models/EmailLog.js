/**
 * EmailApi.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var emailTypes = sails.config.enums.emailLog.types;

module.exports = {

    attributes:{

        to     :{
            type    :'string',
            email   :true,
            required:true
        },
        from   :{
            type    :'string',
            email   :true,
            required:true
        },
        subject:{
            type:'string'
        },
        html   :{
            type:'string'
        },
        custom :{
            type:'string'
        },
        type   :{
            type:'string',
            in  :emailTypes
        }
    }

};

