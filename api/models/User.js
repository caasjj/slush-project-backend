'use strict';

var _ = require( 'lodash' );

/**
 * User.js
 *
 * @module :: User
 *
 *
 * @description :: Waterline model of a user's account.
 *
 * Associations:
 *
 * Profile   : ManyToOne  - user profile information
 * Network   : ManyToMany - table of user's connections to other users
 * Passport  : ManyToOne  - login/auth credentials
 * Messages  : ManyToOne  - user's stored chat messages
 * Logins    : ManyToOne  - record of user's logins
 */

// no 'created_by' attribute for user, as it is self created!
var base = _.cloneDeep( require( '../base/Model' ) );

var userEnums = sails.config.enums.user;

var model = {

    schema:true,

    /*********************************************************
     *
     * Instance Attributes
     *
     *********************************************************/
    attributes:{

        //******************* REQUIRED ***********************//
        /**
         * @param {string} username
         * @description username used by user to manage account
         */
        username:{
            type    :'string',
            required:true,
            unique  :true
        },

        /**
         * @param {string} email
         * @description primary email used for account confirmation email
         */
        email:{
            type    :'string',
            required:true,
            email   :true,
            unique  :true
        },

        /**
         * @param {boolean} emailValid
         * @description set to false if email carrier indicates unable to deliver
         */
        emailValid:{
            type      :'boolean',
            defaultsTo:true
        },

        /**
         * @param {boolean} confirmed
         * @description flag set to true after user clicks on confirmation link in email
         */
        confirmed:{
            type      :'boolean',
            defaultsTo:false
        },

        /**
         * @param {boolean} active
         * @description set to true on confirmation, may be set to false to disable an existing account
         */
        active:{
            type      :'boolean',
            defaultsTo:true
        },

        /**
         * @param {string} hash
         * @description random hash in link sent to user, and received back from the webhook, confirming the account
         */
        hash:{
            type:'string'
        },

        hashType: {
            type: 'string',
            in: sails.config.enums.user.hashType
        },

        hashExpiration:{
            type:'string'
        },

        /**
         * @param {string} qualification
         * @description type of account user belongs to.
         * <br>
         * Can be provider|employee|vendor|serviceProvider
         */
        qualification:{
            type    :'string',
            in      :userEnums.qualificationTypes,
            required:true
        },

        //******************** DEFAULT ***********************//
        /**
         * @param {string} username
         * @description username used by user to manage account
         */
        admin        :{
            type      :'boolean',
            defaultsTo:false
        },

        /**
         * @param {string} membership
         * @description level of user's membership
         * Can be premium|member|freemium
         * @default freemium
         */
        membership:{
            type      :'string',
            in        :userEnums.membershipTypes,
            defaultsTo:'freemium'
        },

        //********************* OPTIONAL ***********************//


        //****************** INSTANCE METHODS ******************//

        /*********************************************************
         *
         * Associations
         *
         *********************************************************/
        // OneToOne relationship with Profile
        /**
         * @param {string} profile
         * @description foreign key to user's Profile model
         */
        profiles  :{
            collection:'profile',
            via       :'user'
        },

        /**
         * @param {object} network
         * @description foreign key to join table with 'Network' model in ManyToMany association
         * the key is named 'user', so this end is called 'network_user' (model_key).
         * <br>
         * Other end is 'user_network', so the join table is 'network_user__user_network'.
         * @see  {@link Network}
         */
        networks:{
            collection:'network',
            via       :'users'
        },

        /**
         * @param {object} passports
         * @description array of passports in OneToMany
         *
         * @param {string} collection: 'Profile' HasMany 'ProfilePhoto' instances
         * @param {string} via: 'ProfilePhoto' instances refer to owner 'Profile' instance via 'profile' attribute
         */
        passports:{
            collection:'passport',
            via       :'user'
        },

        // TODO: delete these boilerplate models, or adapt to my own model structure
        // Message objects that user has sent
        messages :{
            collection:'message',
            via       :'user'
        },

        // Login objects that are attached to user

        logins        :{
            collection:'UserLogin',
            via       :'user'
        },

        // Below are relations to another objects via generic 'createdUser' and 'updatedUser' properties

        // Authors
        createdAuthors:{
            collection:'Author',
            via       :'createdUser'
        },
        updatedAuthors:{
            collection:'Author',
            via       :'updatedUser'
        },

        // Books
        createdBooks  :{
            collection:'Book',
            via       :'createdUser'
        },
        updatedBooks  :{
            collection:'Book',
            via       :'updatedUser'
        }

        /*********************************************************
         *
         * Instance Methods
         *
         * These should ALL be synchronous
         *
         *********************************************************/
    },


    /*********************************************************
     *
     * Model Methods
     *
     *********************************************************/
    //****************** Model Fields *********************//
    greyAttributes:[
        'emailValid',
        'confirmed',
        'active',
        'hash',
        'hashType',
        'hashExpiration',
        'admin',
        'membership'
    ],

    /**
     * @param {integer} userId - id of user instance whose account privilege is to be checked
     *
     * @param {function} callback - function to call on completion
     *
     * @description - returns true if user is an admin account, false otherwise
     */
    isAdmin:function ( userId, callback ) {

        sails

            .models['user']

            .findOne( userId )

            .exec( function ( err, user ) {

                if ( err || !user ) return callback( null, false );
                callback( null, user.admin );

            } );
    },


    /*********************************************************
     *
     * Model Lifecycle Methods
     *
     *********************************************************/
    afterCreate:function afterCreate ( newRecord, cb ) {

        var acl = sails.services.acl,
            userId = newRecord.id;

        acl

            .createUserACL( userId, function ( err ) {

                if ( err ) return cb( err );

                if ( newRecord.admin )
                    {

                        acl

                            .makeAdmin( userId, cb );

                    }
                else
                    {

                        cb();

                    }
            } );
    }

    /*********************************************************
     *
     * Model Types and Validation Methods
     *
     *********************************************************/
};

module.exports = _.merge( base, model );

