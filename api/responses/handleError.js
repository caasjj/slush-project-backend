'use strict';

function parseMessage ( details ) {
    var result = [];
    var temp;

    if ( !_.isArray( details ) )
        {
            details = [details];
        }

    details.forEach( function ( detail ) {
        temp = detail.split( '\n' );
        result.push( (temp[1]).replace( "•", '' ) + ': ' + (temp[2]).replace( "•", '' ) )
    } )

    return result.join( '\n' );
}

module.exports = function handleErrors ( error, options ) {

    var errType = error.constructor.name;

    switch ( errType )
    {
        case 'String':
            error = {
                status :(options && options.status) || 400,
                code   :(options && options.code) || 'E_BADREQUEST',
                message:error,
                details:{}
            }
            break;
        case 'WLValidationError':
            error = {
                status :error.status || (options && options.status) || 400,
                code   :error.code || (options && options.code) || 'E_VALIDATION',
                message:parseMessage( error.details ),
                details:error
            }
            break;
        case 'WLError':
            if ( _.isArray( error.originalError ) )
                {
                    return handleErrors.call( this, error.originalError[0].err );
                }
            else
                {
                    return handleErrors.call( this, error.originalError ); // this is a
                }
        case 'WLUsageError':
            error = {
                status :error.status || (options && options.status) || 400,
                code   :error.code || (options && options.code) || 'E_WLUSAGE',
                message:'A WLUsage error occurred',
                details:error
            }
            sails.log.error( 'Got WLUsageError - passing it on!' );
            break;
        default:
            error = {
                status :error.status || (options && options.status) || 500,
                code   :error.code || (options && options.code) || 'E_UNKNOWN',
                message: error.message || 'An error occurred in the server',
                details:error
            }
            break;
    }

    var req = this.req;
    var res = this.res;
    var sails = req._sails;

    // Set status code
    res.status( error.status );

    // Log error to console
    sails.log.info( 'Error (%s) - code: %s, %s ', error.status, error.code, error.message );

    // Only include errors in response if application environment
    // is not set to 'production'.  In production, we shouldn't
    // send back any identifying information about errors.
    if ( sails.config.environment === 'production' )
        {
            error = { message:error.message };
        }

    // Backend will always response JSON
    return res.jsonx( error );
}