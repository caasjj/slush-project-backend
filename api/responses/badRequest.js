'use strict';

/**
 * 400 (Bad Request) Handler
 *
 * Usage:
 * return res.badRequest();
 * return res.badRequest(data);
 * return res.badRequest(data, 'some/specific/badRequest/view');
 *
 * e.g.:
 * ```
 * return res.badRequest(
 *   'Please choose a valid `password` (6-12 characters)',
 *   'trial/signup'
 * );
 * ```
 */
module.exports = function badRequest ( error, options ) {

    return this.res.handleError( error, {
        status:(options && options.status) || 400,
        code  :(options && options.code) || 'E_BADREQUEST'
    } );
};
