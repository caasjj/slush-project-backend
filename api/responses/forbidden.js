'use strict';

/**
 * 403 (Forbidden) Handler
 *
 * Usage:
 * return res.forbidden();
 * return res.forbidden(err);
 * return res.forbidden(err, 'some/specific/forbidden/view');
 *
 * e.g.:
 * ```
 * return res.forbidden('Access denied.');
 * ```
 */
module.exports = function forbidden ( error, options ) {
    // Get access to `req`, `res`, & `sails`
    return this.res.handleError( error, {
        status:(options && options.status) || 403,
        code  :(options && options.code) || 'E_AUTH'
    } );

};
