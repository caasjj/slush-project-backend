'use strict';

/**
 * 500 (Server Error) Response
 *
 * Usage:
 * return res.serverError();
 * return res.serverError(err);
 * return res.serverError(err, 'some/specific/error/view');
 *
 * NOTE:
 * If something throws in a policy or controller, or an internal
 * error is encountered, Sails will call `res.serverError()`
 * automatically.
 */
module.exports = function serverError ( error ) {
    // Get access to `req`, `res`, & `sails`
    error.status = error.status || 500;
    error.code = 'E_SERVERERROR';
    return this.res.handleError( error );

};
