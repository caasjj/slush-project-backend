'use strict';

/**
 * 200 (OK) Response
 *
 * Usage:
 * return res.ok();
 * return res.ok(data);
 * return res.ok(data, 'auth/login');
 *
 * @param  {Object}         data
 * @param  {String|Object}  options   pass string to render specified view
 */
module.exports = function sendOK ( data, options ) {
    // Get access to `req`, `res`, & `sails`
    var req = this.req;
    var res = this.res;
    var sails = req._sails;
    var status = (options && options.status) || 200;

    sails.log.silly( 'res.ok() :: Sending ', status, '("OK") response' );

    // Set status code
    res.status( status );

    // If appropriate, serve data as JSON(P)
    // Backend will always response JSON
    if ( typeof data === 'string' ) data = JSON.stringify( data );
    return res.jsonx( data );
};
