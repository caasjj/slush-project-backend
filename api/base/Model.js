'use strict';

/**
 * api/base/model.js
 *
 * Base model for all sails.js models. This just contains some common code that every "nearly" every model uses.
 */
module.exports = {
    schema: true,

    attributes: {
        // Relation to User object via created user id
        createdUser: {
            model: 'User',
            columnName: 'createdUserId',
            required: true
        },
        // Relation to User object via updated user id
        updatedUser: {
            model: 'User',
            columnName: 'updatedUserId',
            required: true
        }
    },

    greyAttributes: [],

    clearGreyAttributes: function clearGreyAttributes(data) {
        _.each(this.greyAttributes, function clearGreyAttributesIterate(greyAttribute) {
            delete data[greyAttribute];
        });
    },

    afterCreate: function afterCreate(newRecord, cb) {
        var acl = sails.services.acl;

        var userId = newRecord.createdUser,
            Model = this.identity,
            resourceId = newRecord.id;

        acl

            .giveUserAccessToResource(userId, Model, resourceId, '*', function (err) {

                // returns [undefined] for no error!
                if (err) return  err;

                cb();

            });
    },

    afterDestroy: function (destroyedRecord, cb) {

        var Model = this.identity,
            resourceId = destroyedRecord.id;

        if (_.isArray(destroyedRecord) && destroyedRecord.length) {
            _.forEach(destroyedRecord, function(record) {

                sails.log.info('Deleted model %s, id %d: ', this.identity, record.id);
                sails.services.acl.deleteResource( this.identity, record.id, cb );

            }, this);
        }
        else {

            sails.log.info('Deleted model %s, id %d: ', this.identity, destroyedRecord);
            if (!resourceId) return cb();
            sails.services.acl.deleteResource( Model, resourceId, cb );
        }
    }

};
