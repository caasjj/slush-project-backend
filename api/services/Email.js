'use strict';
/**
 * Email Service
 * (sails.services.acl)
 *
 * A service to send email through an external mail provider.  The service works either by standard node callbacks,
 * or an event api.  All sent emails are logged in 'emailLog' model.  For testing purposes, it is also possible to
 * invoke the send method without actually sending an email by setting 'sails.config.email.bypass' to true.
 * Configuration is done in 'config/email.js' and api keys can be provided in 'config/local.js'.
 *
 *
 */
var mail = require('./emailProviders');

// this api allows either for a normal callback, or if
// callback is omitted, event api
function sendMail(message, cb) {

    // if bypass is set, just return
    if (sails.config.email.bypass) return cb();

    // if no from, use default from config
    message.from = message.from || sails.config.email.defaultSender;

    mail

        .send(message, function (err, body) {

            if (err) return cb(err);
            body = mail.parse(body);

            // log the email
            sails

                .models['emaillog']

                .create({
                    to     : message.to,
                    from   : message.from,
                    subject: message.subject,
                    html   : message.html,
                    custom : JSON.stringify(message.custom),
                    type   : message.type
                })

                .exec(function (err) {

                    if (err) sails.log.warn('Could not save email ', message, ' to email log.');

                    // emit a 'sent' event only if email has an association custom:action
                    if (message.custom && message.custom.action && !cb) {
                        sails.emit('email:' + message.custom.action + ':sent', body);
                    }
                    else {
                        return cb(null, body);
                    }
                });


        });
}


var template = function (to, type, hash) {

    var email;
    var url = sails.config.staticServerUrl;

    switch (type) {

        case 'confirmation':
            email = generateEmail('Confirm Account');
            break;

        case 'passwordReset':
            email = generateEmail('Reset Password');
            break;

        case 'reActivation':
            email = generateEmail('Re-activate Account');
            break;

        default:
            email = null;
            break;

    }


    function generateEmail(linkLabel) {

        var html = '<p>' +
            'Click the link to ' +
            '<a href="' + url + '/' + type + '?h=' + hash + '">' + linkLabel + '</a>' +
            '</p>';

        return  {
            to     : to,
            subject: linkLabel,
            html   : html,
            type   : type,
            custom : {}
        };
    }

    return email;
};

module.exports = {

    send: function (message, cb) {

        sendMail(message, cb);

    },

    emitEvent: function (data) {

        // parse any custom data on data.custom
        var data = mail.parse(data);

        if (!data.custom.action) return;

        // event in ['sent', 'delivered', 'clicked', 'bounced', 'dropped']
        var eventName = 'email:' + data.custom.action + ':' + data.event;

        sails

            .emit(eventName, data);

    },

    verifyWebHook: function (message, cb) {

        cb(mail.verifyWebHook(message));

    },

    template: template

}