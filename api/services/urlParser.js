'use strict';
/**
 * UrlParser Service
 * (sails.services.urlParser)
 *
 * A service to parse a url in the form '/model/:id/association/:a_id'. This
 * functionality is used in ACL policies.
 *
 *  //TODO: write tests for this urlParser service
 */

/***
 * function getModel - return 'model' parameter of the url, i.e. the model whose controller
 * action will be invoked by the request.
 *
 * @param request
 * @returns {string} model name
 */
module.exports.model = function getModel ( request ) {
    return request.options.model || request.options.controller;
}

/***
 * function getAssociation - returns the name of the model corresponding to the association
 * part of the URL. i.e. given /model/:id/association, returns 'association'.
 *
 * @param request
 * @returns {string} model name corresponding to the associated model
 */
module.exports.association = function getAssociation ( request ) {
    var alias = request.options.alias;

    if (!alias) return null;

    // fetch model associated with the alias
    var assoc = _.find( request.options.associations, function(association) {
        return association.alias === alias;
    } ).collection;

    return assoc;
}

/***
 * function userId - id of a user resource in the url, wherever it may appear. i.e.
 * given either /user/:u_id, or /user/:u_id/model/:id or /model/:id/user/:u_id will
 * all return u_id.
 *
 * @param request
 * @returns {number} id of user model appearing in the url
 */
module.exports.userId = function parseUserId ( request ) {
    var userId;
    if ( request.options && request.options.model === 'user' )
        {
            userId = request.param( 'parentid' ) || request.param( 'id' );
        }
    else if ( request.options && request.options.alias === 'user' )
        {
            userId = request.param( 'id' );
        }
    return +userId;
}

/***
 * function parseAssociationId - returns id of associated object, i.e. given
 * /model/:id/association will return NaN, and given /model/:id/association/:a_id
 * will return a_id
 *
 * @param request
 * @returns {number}
 */
module.exports.associationId = function parseAssociationId ( request ) {
    return +(request.options && request.options.alias && request.param( 'id' ));
}

/***
 * function parseModelId - returns id of model, i.e. given
 * /model/:id or /model/:id/association or /model/:id/association/:a_id will all
 * return id.
 *
 * @param request
 * @returns {number}
 */
module.exports.modelId = function parseModelId( request ) {
    return +(request.param('parentid') || request.param('id'));
}