'use strict';
/**
 * ACL Service
 * (sails.services.acl)
 *
 * A service to implement role/role-permission based access to various resources.
 * Roles are assigned specific permissions on various resources. Roles can also be
 * children of parent roles, in which case the children inherit all the permissions
 * of the parents.  Users assigned to certain roles gain those permissions, and
 * Users assigned multiple roles gain the Union of all those roles.
 *
 * Uses: https://github.com/OptimalBits/node_acl
 *
 * Mongo tables created:
 *
 ***** acl_meta (list roles and users):
 *
 * Two rows that sore a list of "roles" and "user id's"
 *
 * key:"roles", "role_1":true, "role_2": true, ..
 * key:"users", "1":true, "2":true, ...
 *
 * example:
 * key:"roles", "user_1":true, "user_2":true, "user_3":true, "network_1":true
 * key:"users", "1":true, "2":true, "3":true
 *
 ***** acl_users (map id to roles):
 *
 * Each row stores a list of roles for a given user id
 *
 * key:"user1 id", "role_x":true, "role_y":true
 * key:"user2 id", "role_r":true, "role_s":true
 *
 * example: (user with id 1 has roles user_1, profile_1 and network_123)
 * key:"1", "user_1":true, "profile_1":true, "network_1213":true
 *
 * ***** acl_resources (map role to accessible resources)
 *
 * Each row stores a list of resources with -some- access for a given role, but need to inspect acl_allows_<resource>
 * to see the actual list of permissions
 *
 *   key:<role_1>, <resource_1>:true, <resource_2>:true ...s
 *   key:<role_2>, <resource_1>:true, <resource_x>:true, ...
 *
 * example: (user_1 has access to his accounts, his network has access to his profile - don't know what access0
 *
 *   key:"user_1", "user_1":true, "profile_1":true, "network_1":true
 *   key:"network_1", "profile_1":true
 *
 ***** acl_allows_<resourceName> (map role to permissions for resourceName)
 *
 * Each row stores a list of permissions for a given role for the resource in <resourceName>
 *
 * Note: not smart enough to remove all other permissions if '*' granted to a role
 *
 *   key:<role_1> <access_1>:true, <access_2>: true, ...
 *   key:<role_2> <access_1>:true, <access_N>: true, ...
 *
 * example: (member role has read and write access to resourceName)
 *   key:"member", "read": true, "write": true
 *
 */
var NodeAcl = require('acl');
var mongodb = require('mongodb');
var redis = require('redis');
var contract = require('./utils/contract');  // function parameter type verification
var _ = require('lodash');

contract.debug = true;

/****
 * function ACL - exported module object used to initialize the service
 * @param cb
 * @constructor
 */

function ACL(cb) {

    var config = sails.config.acl;

    _connect(function (err) {

        if (err) return cb(err);
        if (config.migrate !== 'drop') return cb();


        // drop and re-initialize the ACL store
        ACL.

            _backend

            .clean(function (err) {


                if (err) return cb(err);

                _createRoles(cb);

            });

    });

}

/***
 * function addUserToNetwork - add a user to the user's network
 *
 * @param userId - id of network owner
 * @param id - id of user to be added to network
 * @param cb
 */
ACL.addUserToNetwork = function addUserToNetwork ( userId, id, cb ) {

    var network = _getUserNetwork(userId);

    ACL

        ._acl

        .addUserRoles(id, network, cb);
};

/***
 * function removeUserFromNetwork - remove a user to the user's network
 *
 * @param userId - id of network owner
 * @param id - id of user to be removed from network
 * @param cb
 */
ACL.removeUserFromNetwork = function removeUserFromNetwork ( userId, id, cb ) {

    var network = _getUserNetwork(userId);

    ACL

        ._acl

        .removeUserRoles(id, network, cb);
};

/***
 * function getUserNetwork - read a user's network
 *
 * @param userId - id of network owner
 * @param cb
 */
ACL.getUserNetwork = function getUserNetwork ( userId, cb ) {

    var network = _getUserNetwork(userId);

    ACL

        ._acl

        .roleUsers(network, cb);
};

/***
 * function isInUserNetwork - true only if user in the network
 *
 * @param userId - id of network owner
 * @param id - id of user to be checked for inclusion in the network
 * @param cb
 */
ACL.isInUserNetwork = function isInUserNetwork ( userId, id, cb ) {

    var network = _getUserNetwork(userId);

    ACL

        ._acl

        .hasRole(id, network, cb);
};

/***
 * function isAdmin - returns true if user has admin role
 *
 * @param {integer} id - user id to check
 * @param {function} cb - callback on completion
 */
ACL.isAdmin = function isAdmin(id, cb) {


    try {
        ACL.

            _acl

            .hasRole(id, 'admin', cb);
    }
    catch (e) {
        cb(null, false);
    }
}

/***
 * function createUserACL - creates basic ACL rules of a user by
 *
 *   1. creating a unique user role (user_<id>)
 *   2. giving that role '*' access to the user's 'user' resource (also user_<id>)
 *   3. making user's role a child of 'public' role.
 *
 *
 * @param userId
 * @param cb
 */
ACL.createUserACL = function createUserACL(userId, cb) {

    var userRole = _getUserRole(userId);

    ACL.giveUserAccessToResource(userId, 'user', userId, ['*','read','write','update'], function (err) {

        // returns [undefined] for no error!
        if (err) return  err;

        ACL

            ._acl

            .addRoleParents(userRole, 'member')

            // on completion of above, add the user to the created role
            .then(function accessGranted(err) {

                // returns [undefined] for no error!
                if (_.compact(err).length) return  err;

                return ACL._acl.addUserRoles(userId, userRole);

            })

            .then(cb)

    });

};

/***
 * function giveUserAccessToUniqueResource - as the name says, grants 'action' permission to individual role
 * 'user_x' and 'admin', on 'resource_y' where the role and resource are identified by _getUserRole and
 * _getUniqueResource above
 *
 * This function is generally used to give a user role (user_<id>) all access ('*') to resources he owns.  For sharing
 * resources, it is better to join users to a common role and give that role 'read'/'write' access to the resource.
 *
 *
 * @param {integer} userId - id of the user to create a role for
 * @param {string} resource - name/model of the resource
 * @param {integer} resourceId - id of the specific resource
 * @param {(string|string[])} actions - right/permission to assign
 * @param {function} cb - function called on completion
 */
ACL.giveUserAccessToResource = function giveUserAccessToResource(userId, resource, resourceId, actions, cb) {
    contract(arguments)
        .params('number', 'string', 'number', 'string|array', 'function')
        .end();

    var userRole = _getUserRole(userId),
        userResource = _getUniqueResource(resource, resourceId);

    actions = _.isArray(actions) ? actions : [actions];

    ACL

        ._acl

        // create the user role and resource identifier and assign the action permission

        .allow([
            {
                roles : ['admin', userRole],
                allows: [
                    {
                        resources  : [userResource],
                        permissions: actions
                    }
                ]
            }
        ])

        // done + catchall error
        .then(function (err) {

            // returns [undefined] for no error!
            if (_.compact(err).length) return  err[0];
            cb();
        })

};

/***
 * function giveNetworkAccessToResource - as the name says, grants 'action' permission to individual role
 * 'user_x' and 'admin', on 'resource_y' where the role and resource are identified by _getUserRole and
 * _getUniqueResource above
 *
 * This function is generally used to give a user role (user_<id>) all access ('*') to resources he owns.  For sharing
 * resources, it is better to join users to a common role and give that role 'read'/'write' access to the resource.
 *
 *
 * @param {integer} userId - id of the user to create a role for
 * @param {string} resource - name/model of the resource
 * @param {integer} resourceId - id of the specific resource
 * @param {(string|string[])} actions - right/permission to assign
 * @param {function} cb - function called on completion
 */
ACL.giveNetworkAccessToResource = function giveNetworkAccessToResource(userId, resource, resourceId, actions, cb) {
    contract(arguments)
        .params('number', 'string', 'number', 'string|array', 'function')
        .end();

    var networkRole = _getUserNetwork(userId),
        userResource = _getUniqueResource(resource, resourceId);

    actions = _.isArray(actions) ? actions : [actions];

    ACL

        ._acl

        // create the user role and resource identifier and assign the action permission

        .allow([
            {
                roles : [networkRole],
                allows: [
                    {
                        resources  : [userResource],
                        permissions: actions
                    }
                ]
            }
        ])

        // done + catchall error
        .then(function (err) {

            // returns [undefined] for no error!
            if (_.compact(err).length) return  err[0];
            cb();
        })

};

/***
 *
 * @param resource
 * @param cb
 */
ACL.deleteResource = function deleteResource( resource, resourceId, cb) {
    contract(arguments)
        .params('string', 'number|string', 'function')
        .end();

    var uniqueResource = _getUniqueResource(resource, resourceId);
    ACL

        ._acl

        .deleteResource( uniqueResource, cb);
};

/***
 * function isAllowed - returns true if the specific user id is permitted access to a specific resource
 * @param {integer} userId - user id of the user to check
 * @param {string} resourceName - name/model of the resource
 * @param {integer} resourceId - id of the resource
 * @param {(string|string[])} actions - actions (permissions) to check
 * @param {function} cb on completion
 */
ACL.isAllowed = function isAllowed(userId, resourceName, resourceId, actions, cb) {

    contract(arguments)
        .params('number', 'string', 'number', 'string|array', 'function')
        .end();


    var resource = _getUniqueResource(resourceName, resourceId);

    ACL

        ._acl

        .isAllowed(userId, resource, actions, function (err, allowed) {
            if (err) return cb(err);
            cb(null, allowed);

        });
};

/***
 * function whatReadableItems
 *
 * @param userId
 * @param resourceName
 * @param cb
 * @returns {*}
 */
ACL.whatReadableItems = function readInstances(userId, resourceName, cb) {

    contract(arguments)
        .params('number', 'string|Array', 'function')
        .end();

    var userRoles = ACL._acl.userRoles(userId);

    var userResources = userRoles.then(function (roles) {

        return ACL._acl.whatResources(roles, ['*', 'read'])
    });

    return userResources.then(function (resources) {

        var output = [];

       resources.forEach( function(resource) {
           if (resource.match(resourceName)) {
               output.push( resource.replace(resourceName + '_', '') );
           }
       });

//        cb(null,  _.uniq( output ) );
        cb(null,  output );

    });
};

/***
 * function makeAdmin - gives a user admin privileges
 * @param {integer} id - user id to gain admin privilege
 * @param {function} cb - callback on completion
 */
ACL.makeAdmin = function makeAdmin(id, cb) {

    ACL

        ._acl


        .addUserRoles(id, 'admin', cb);
}

/****************************************************************************
 *
 * Private Functions
 *
 ****************************************************************************/

/***
 * function _createRoles: Define initial roles and permissions
 * @param cb - callback when done
 * @private
 */
function _createRoles(cb) {

    // give admin access to
    cb();

}

/***
 * function _getUserRole: Defines a role for a specific user, as 'user_[:id]', where :id is the user's id
 *
 * @param {integer|string} id - user's id
 * @returns {string} - user role
 * @private
 */
function _getUserRole(id) {

    return 'user_' + id;

}

/***
 * function _getUserNetwork: Defines a resource identifying a user's network
 *
 * @param {integer|string} id - user's id
 * @returns {string} - user role
 * @private
 */
function _getUserNetwork ( id ) {

    return 'network_' + id;

}

/***
 * function _getUniqueResource: Defines a unique resource based on id of the resource, as '[resource]_[:id]'
 * This is so that a given role can be assigned permissions over a single resource, in order to implement
 * network/group sharing of individual user owned resources
 *
 * @param {string} resource - name (typically Model.identity) of the resource
 * @param {integer|string} id - resource id
 * @returns {string} - resource identifier
 * @private
 */
function _getUniqueResource(resource, id) {

    return resource + '_' + id;

}

/***
 * function connect - connect and initialize the ACL service from config (/config/local.js)
 *
 * @param {function} cb - function called upon completion
 */
function _connect(cb) {

    ACL._mongodb = mongodb;

    if (sails.config.acl.backend === 'mongodb') {
        var connection = "mongodb://" + sails.config.acl.mongodb.url + '/' + sails.config.acl.mongodb.database;

        mongodb

            .MongoClient

            .connect(connection, function (error, db) {

                if (error) return cb(error);

                ACL._backend = new NodeAcl.mongodbBackend(db, 'acl_');
                ACL._acl = new NodeAcl(ACL._backend);
                ACL._db = db;

                cb();

            });

    }

    else if (sails.config.acl.backend === 'redis') {

        ACL._client =

            redis

                .createClient(sails.config.acl.redis.port, sails.config.acl.redis.url, { no_ready_check: sails.config.acl.redis.no_ready_check });

        ACL._backend = new NodeAcl.redisBackend(ACL._client);

        ACL._acl = new NodeAcl(ACL._backend);

        cb();
    }

    else {
        ACL._backend = new NodeAcl.memoryBackend();
        ACL._acl = new NodeAcl(ACL._backend);
        ACL._db = null;

        cb();
    }


}


module.exports = ACL;