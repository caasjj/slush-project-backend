'use strict';

module.exports = {

    send:function ( message, callback ) {
        sails.log.warn( 'Failed to send ', message, ' with mailchimp provider. No supported yet.' );
        callback( 'Failed to send ', message, ' with mailchimp provider. No supported yet.' )
    },

    verifyWebHook:function ( message ) {
        sails.log.warn( 'Failed to verify ', message, ' with mailchimp provider webhook. No supported yet.' );
        return false;
    }
};