'use strict';

var config = {
    apiKey   :sails.config.email.mailgun.apiKey,
    domain   :sails.config.email.mailgun.domain,
    customKey:sails.config.email.mailgun.customKey
}
var mailgun = require( 'mailgun-js' )( config );

module.exports = {

    send:function ( message, callback ) {

        if ( message.custom ) message[config.customKey] = JSON.stringify( message.custom );
        message.from += '@' + config.domain;
        // TODO: we could do _.omit(message, 'custom') if that extra field ends up problematic
        mailgun.messages().send( message, callback );
    },

    parse        :function ( mailgunResponse ) {

        if ( mailgunResponse[config.customKey] )
            {
                mailGunResponse.custom = JSON.parse( mailgunResponse[config.customKey] );
                delete mailgunResponse[config.customKey];
            }
        else if ( mailgunResponse.custom )
            {
                mailgunResponse.custom = JSON.parse( mailgunResponse.custom );
            }
        ;

        if ( mailgunResponse['message-id'] )
            {
                mailgunResponse.emailId = mailgunResponse['message-id'].replace( '<', '' ).replace( '>', '' );
            }
        else if ( mailgunResponse['Message-Id'] )
            {
                mailgunResponse.emailId = mailgunResponse['Message-Id'].replace( '<', '' ).replace( '>', '' );
            }
        else if ( mailgunResponse.id )
            {
                mailgunResponse.emailId = mailgunResponse.id.replace( '<', '' ).replace( '>', '' );
            }

        return mailgunResponse;
    },

    // is it bad design to have synchronous and asynchronous methods in an api?
    verifyWebHook:function ( message ) {
        if ( !message.timestamp || !message.token || !message.signature )
            {
                return 'Mailgun message cannot be authenticated - missing fields';
            }
        // this kinda sucks, because we're having to return a true for an error
        return mailgun.validateWebhook( message.timestamp, message.token, message.signature ) ? null :
            'Mailgun message cannot be authenticated - signature mismatch';
    }
};