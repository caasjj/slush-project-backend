module.exports = function ( sails ) {


    function talk () {
        console.log.apply( null, arguments );
    }

    return {
        configure:function () {
            console.log( 'Configuring sails hook' );
        },

        initialize:function ( cb ) {

            var myEvents = [
                'hook:orm:loaded',
                'hook:controllers:loaded',
                'hook:pubsub:loaded'
            ];

            sails.after( myEvents, function () {
                // for some reason, this triggers before all of the events
                // have individually triggered - right before the last one
                talk( 'All done!', new Date() );
                return cb();
            } );

            myEvents.forEach( function ( event ) {
                sails.after( event, function () {
                    talk( 'Event: ', event, new Date() );
                } );

            } );
        },

        route:{

            before:{
                '/*':function ( req, res, next ) {
                    next(); // this runs before the controller logic
                }

            },

            after:{
                '/*':function ( req, res, next ) {
                    next();  // this runs after the controller logic
                }

            }

        }
    }

}