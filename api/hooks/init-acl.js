'use strict';

/**
 * load-db.js
 *
 * This file contains a custom hook, that will be run after sails.js orm hook is loaded. Purpose of this hook is to
 * check that database contains necessary initial data for application.
 */
var acl = require( 'acl' );
var mongodb = require( 'mongodb' );

module.exports = function hook ( sails ) {

    return {

        /**
         * Method that runs automatically when the hook initializes itself.
         *
         * @param   {Function}  next    Callback function to call after all is done
         */
        initialize:function initialize ( next ) {
            var hook = this;

            // Wait for sails orm hook to be loaded
            sails.after( 'hook:orm:loaded', function ormHookLoaded () {

                sails.log.info( 'Initializing ACL!' );

                sails.services.acl( function initializeAcl(err) {

                    sails.emit('hook:acl:loaded')
                    sails.log.info( 'ACL Initialized' );

                    next(err);

                });

            } );
        }
    }
};