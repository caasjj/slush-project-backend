/**
 * TestEmailController
 *
 * @description :: Server-side logic for managing testemails
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    // this will not be exposed in production
    // tag email with :
    send:function ( request, response ) {
        var parsedErr;
        var emailService = sails.services['email'];
        var data = request.params.all();

        emailService.send( data, function ( err, email ) {
            if ( parsedErr = sails.services['modelerrorparser'].parseError( err ) )
                {
                    return response[parsedErr.method]( parsedErr.data );
                }
            response.ok( email, {status:201} );
        } );
    },

    /***************************************************************
     *
     * Webhooks
     * @param request
     * @param response
     ***************************************************************/
    // implement webhook validation
    hookHandler:function ( request, response ) {

        //sails.log.info('Received a mgHook call: ', request.params.all() );

        var data = request.params.all();
        var emailService = sails.services['email'];

        emailService.emitEvent( data );
        response.ok();

    }
};

