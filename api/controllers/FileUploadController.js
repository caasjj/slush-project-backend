/**
 * FileUploadController
 *
 * @description :: Server-side logic for managing Fileuploads
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    upload:function ( req, res ) {

        var blobAdapter = require('skipper-disk')();
        var receiving = blobAdapter.receive();
        req
            .file('junk').upload(  receiving, function ( err, files ) {
                if ( err )
                    {
                        //                        return res.badRequest( { code: err.code, message: err.message } );
                        res.negotiate(err);
                    }

                return res.json( {
                    message:files.length + ' file(s) uploaded successfully!',
                    files  :files
                } );
            } );
    },

    download: function (req,res) {
        var blobAdapter = require('skipper-disk')();
        var filename = req.param('f');
        blobAdapter.read(filename, function(err, data) {
            console.log(data);
            res.send(data);
        });
    },

    /**
     * FileController.download()
     *
     * Download a file from the server's disk.
     */
    downloadStream: function (req, res) {
        require('fs').createReadStream(req.param('path'))
            .on('error', function (err) {
                return res.serverError(err);
            })
            .pipe(res);
    }

};

