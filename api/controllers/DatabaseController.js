/**
 * DatabaseController - a complete TEMPORARY CLOOGE for use during development
 * to put the database in a known state without bringing down the server
 *
 * @description :: Server-side logic for managing Databases
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    zap:function ( request, response ) {

//        var deleteFuncs = ['network', 'network_user__user_network', 'profileemail', 'profilephoto'
//        ].map( function ( model ) {
//            return function ( cb ) {
//                sails.models['user'].query( 'DELETE FROM ' + model, function ( err ) {
//                    if ( err ) return cb( err );
//                    cb( null, model + ' emptied' );
//                } );
//            }
//        } );

        var deleteFuncs = [];

        deleteFuncs.push( function ( cb ) {
            sails.models['profile'].query( 'DELETE FROM profile WHERE id > 3', function ( err ) {
                if ( err ) return cb( err );
                cb( null, 'profile with id>3 deleted' )
            } );
        } );

        deleteFuncs.push( function ( cb ) {
            sails.models['user'].query( 'DELETE FROM user WHERE id > 3', function ( err ) {
                if ( err ) return cb( err );
                cb( null, 'user with id>3 deleted' )
            } );
        } );

        deleteFuncs.push( function ( cb ) {
            sails.models['passport'].query( 'DELETE FROM passport WHERE id > 3', function ( err ) {
                if ( err ) return cb( err );
                cb( null, 'passport with id>3 deleted' )
            } );
        } );

        async.parallel( deleteFuncs, function ( err, results ) {
            if ( err ) return response.badRequest( err );
            response.ok( results );
        } );

    }
};

