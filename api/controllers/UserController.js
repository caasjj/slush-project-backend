'use strict';

/**
 * UserController
 *
 * @description :: Server-side logic for managing creation of user accounts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var uuid = require('node-uuid');
var crypto = require('crypto');
var async = require('async');

var waterfall = async.waterfall;


module.exports = _.merge(_.cloneDeep(require('../base/Controller')), {

    register: function register(request, response) {
        waterfall([

            createAccount(request),
            connectPassport

        ],
            function registerCallback(err, result) {
                if (err) return response.serverError(err);
                response.ok(result, {status: 201});
            });
    },

    requestEmail: function requestConfirmation(request, response) {

        var hashType = request.param('hashType') || 'confirmation';
        if (sails.config.enums.user.hashType.indexOf(hashType) < 0) return cb('Invalid hashType');

        if (!request.user) return response.badRequest('Cannot find user');

        if (request.user.confirmed && hashType === 'confirmation') return response.badRequest('User already confirmed');

        updateUserHash(request.user, hashType, function userHashUpdated(err, user) {

            // send response back to client before waiting for email to go through
            // hash has already been updated
            response.ok('Email requested.');

            // send the email ... and hope for the best!
            generateEmail(user, hashType);
        });
    },

    confirmation: function confirmation(request, response) {

        var hash = request.param('h');

        if (!hash) return response.badRequest('Confirmation hash missing from request');

        waterfall([

            verifyHash(hash, 'confirmation'),

            confirmUser

        ],

            function confirmationCallback(err) {

                if (err) response.badRequest(err);
                response.ok('Your account has been confirmed!');

            });
    },

    passwordReset: function passwordReset(request, response) {

        var hash = request.param('h');
        var password = request.param('password');

        if (!hash) return response.badRequest('Reset hash missing from request');

        waterfall([

            verifyHash(hash, 'passwordReset'),

            function (user, cb) {
                updatePassword(user, password, cb)
            },

            clearHash

        ],
            function passwordResetCallback(err) {

                if (err) response.badRequest(err);
                response.ok('Your password has been updated.');

            });
    }
});

function createAccount(request) {

    return function (cb) {

        var userData = request.params.all();

        waterfall([

            function (_cb) {

                sails

                    .models['user']

                    .create(userData, function (err, user) {

                        if (err) return _cb(err);
                        _cb(null, user);

                    });
            },

            function (user, _cb) {

                request

                    .login(user, function (err) {

                        if (err) return _cb(err, {status: 500});
                        _cb(null, request);

                    });

            } ], cb);
    };
}

function connectPassport(request, cb) {

    var passportService = sails.services['passport'],
        localProtocol = passportService.protocols.local;

    localProtocol

        .connect(request, null, function (err, user) {

            if (err) {
                sails

                    .models['user']

                    .destroy(request.user.id)

                    .exec(function (usererr) {

                        if (usererr) return cb(usererr);
                        return cb(err);

                    });
            }
            else {
                cb(null, user);
            }
        });

}

function updateUserHash(user, hashType, callback) {

    if (sails.config.enums.user.hashType.indexOf(hashType) < 0) return cb('Invalid hashType');

    var hash = generateHash();

    sails

        .models['user']

        .update({id: user.id}, {
            hash          : hash,
            hashType      : hashType,
            hashExpiration: (Date.now())
        })

        .exec(function userUpdated(err, user) {
            sails.log.debug('Updated hash to ', hash + hashType);
            if (err) callback(err, {status: 500});
            if (!user) callback('Could not create email for user', {status: 500});

            callback(null, user[0]);

        });

    function generateHash() {

        return (crypto

            .createHash('sha1')

            .update(uuid.v1() + sails.config.hash.secret)

            .digest('hex')
            );

    }
}

function generateEmail(user, type, cb) {

    if (!user.emailValid) return cb('Email address is not valid. Please update account.');

    if (sails.config.enums.user.hashType.indexOf(type) < 0) return cb('Invalid hashType');
    // send the confirmation email
    var emailService = sails.services['email'];


    var email = emailService.template(user.email, type, user.hash);
    // send the email, using emailService's callback api
    emailService

        .send(email, function $mailSent(err) {

            // store the record in AccountLog
            if (err) return sails.log.error('Could not send the email ', email);
            sails.log.info('%s email sent to user %s at %s', type, user.username, user.email);

            // cb is optional so we can use the event API rather than the callback API
            if (typeof cb === 'function') cb();

        });

}

function verifyHash(hash, type) {

    var where = {
        hash    : hash,
        hashType: type
    };

    return function (cb) {

        sails

            .models['user']

            .findOne(where, function (err, user) {

                if (err) return cb(err);
                if (!user) return cb('Invalid email code. Please request a new email.!');

                var hashAge = Date.now() - user.confirmationHashExpiration,
                    hashExpired = hashAge > sails.config.hash.expiration;

                if (hashExpired) return cb('Email Expired. Please request new email.');

                cb(null, user);

            });
    };
}

function confirmUser(user, cb) {

    var

        where = {
            id: user.id
        },

        update = {
            confirmed: true,
            hashType : 'used'
        };

    sails

        .models['user']

        .update(where, update)

        .exec(cb);

}

function updatePassword(user, password, cb) {

    var
        where = {
            user: user.id
        },

        update = {
            password: password
        };

    sails

        .models['passport']

        .update(where, update)

        .exec(cb);
};

function clearHash(passport, cb) {

    var
        where = {
            id: passport[0].user
        },

        update = {
            hashType: 'used'
        };

    sails

        .models['user']

        .update(where, update)

        .exec(cb);
};
