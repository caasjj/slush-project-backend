
function createAccount(userData, callback) {

    async.waterfall( [

        function ( cb ) {
            sails.models['user'].create( userData, function ( err, user ) {
                if ( err ) return cb( err );
                cb( null, user );
            } );
        },

        function ( user, cb ) {
            request.login( user, function ( err ) {
                if ( err ) return cb( err, {status:500} );
                cb( null, user );
            } );
        },

        function ( user, cb ) {
            sails.services['passport'].protocols.local.connect( request, response, function ( err, user ) {
                if ( err )
                    {
                        sails.models['user'].destroy( user.id ).exec( function ( usererr ) {
                            if ( usererr ) return cb( usererr );
                            return cb( err );
                        } );
                    }
                else
                    {
                        cb( null, user );
                    }
            } );
        }
    ], callback);

}

function validateUserCredentials(credentials, callback) {

    async.waterfall( [

        /***
         *
         * @param cb
         */
        function fetchUserByIdentifier( cb ) {

            sails
                .models['user']
                .findOne( {'username':credentials.identifier} )
                .populate( 'passports' )
                .exec( function ( err, user ) {
                    if ( err ) return cb( err );
                    if ( !user ) return cb( 'Invalid User' );
                    cb( null, user );
                } );
        },

        /***
         *
         * @param user
         * @param cb
         */
        function verifySubmittedPassword( user, cb ) {

            // only use local protocol for account verification
            var passport = _.find(user.passports, function(passport) {
                return passport.protocol === "local";
            });

//            for ( var i = 0; i < user.passports.length; i++ )
//                {
//                    if ( user.passports[i].protocol === "local" )
//                        {
//                            passport = user.passports[i];
//                            break;
//                        }
//                }

            passport.validatePassword( credentials.password, function ( err, matched ) {
                if ( err || !matched )
                    {
                        cb( 'Invalid Password', {status:401} );
                    }
                else
                    {
                        cb( null, user );
                    }
            } );
        }],

        /***
         * final callback indicating whether username/password checked out
         */
        callback);
}

module.exports = {
    createAccount: createAccount,
    validateUserCredentials: validateUserCredentials
};