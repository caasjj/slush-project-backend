'use strict';

/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Maps various middleware functions, whether for ACL or other functionality, to various controller actions.
 * Resources are generally owned by individual users, who have complete control over the resource.
 *
 * Users then add other users in their network and share their resources with members of their network.
 * Alternatively they may create groups, to which they can assign other users from their network, and then
 * share resources with this sub-group.
 *
 */
var _ = require('lodash');

/***
 * Basic Policies used to compose more extensive policies
 *
 */
var

// initialize passport
    pass = ['passport'],

// read and authenticate the token
    token = pass.concat(['getToken']),

// authenticated token only beyond this point
    authenticated = token.concat('authenticated'),

// add data creation meta data to logged in user
    userCreateMeta = authenticated.concat('addCreateMetadata');

/***
 * Policy used to fetch a user based on valid username/password
 * used to send confirmation email to confirm account
 */
var fetchUser = pass.concat('fetchUser');

/***
 * Policy used to filter collection to list of item with read permission by current user
 */
var aclFilterReadables = authenticated.concat('aclFilterReadables');

/***
 * Policy used to limit access to some actions only to admin users
 */
var
    aclAdminOnly = authenticated.concat('aclAdminOnly');

/****
 *  ACL policies used across resource classes
 */
var

// policy used to remove non-directly writable parameters, on non-user account resource
// user account uses register policy, because we cannot user userCreateMeta policy before creating user
    aclSanitizeData = userCreateMeta.concat('clearGreyAttributes'),

// policy to set 'user' attribute to an associated instance so it's associated to current user
    aclAssociateToUser = aclSanitizeData.concat('associateToUser'),

// policy used to check if requester id has 'read' privilege on specific resource
    aclCanViewResource = authenticated.concat('aclCanViewResource'),

// policy used to check if requester has 'update' privilege on specific resource (probably owner only)
    aclCanUpdateResource = aclSanitizeData.concat('aclCanUpdateResource'),

// policy used to check if requester can add/remove child models to the resource (e.g. a profile to a user account)
    aclCanAssociateResources = aclSanitizeData.concat(['aclCanAssociateResources']),

// policy used to check to see if request can delete a resource
    aclCanDeleteResource = authenticated.concat('aclCanDeleteResource');

/***
 *  User Registration policies
 */
var

// policy to remove grey data before creating the user account (pass policy sets createdUser/updatedUser to -1)
    registerUser = token.concat(['clearGreyAttributes', 'addCreateMetadata' ]);

/***
 *  Email webhook endpoint for validating/authenticating incoming webhook requests
 *
 *  Webhooks are callbacks initiated by mail provider to signal the app of events on lifetime of sent mail.
 *  These need to be validated for authenticity from the carrier. This policy does that before forwarding the
 *  POSTed data to the 'hookHandler' action in 'api/controllers/EmailController.js'
 */
var

    emailWebhookValidate = 'emailWebhookValidate';

/***
 * Define Resource Classes to be applied to various resource controllers
 */
var resourceClass = {

    // resources not owned by any one user and not under any ACL - probably not very useful!
    generic  : {
        'create'  : userCreateMeta,      // any authenticated user can create, and createdUser will be set
        'find'    : authenticated,       // any authenticated user can read entire collection
        'findOne' : authenticated,       // any authenticated user can read specific item
        'update'  : userCreateMeta,      // any authenticated user can update, and updatedUser will be set
        'destroy' : authenticated,       // any authenticated user can delete
        'populate': authenticated,       // any authenticated user can read associated data
        'add'     : userCreateMeta,      // any authenticated user can associate children
        'remove'  : authenticated        // any authenticated user can remove association with children
    },

    // user account
    user     : {
        'create'       : aclSanitizeData,            // create user account, removing grey data unless admin
        'find'         : aclFilterReadables,          // return all user accounts with isAllowed by current user
        'findOne'      : aclCanViewResource,         // apply 'aclCanViewResource' policy
        'update'       : aclCanUpdateResource,
        'destroy'      : aclCanDeleteResource,
        'populate'     : aclCanViewResource,
        'add'          : aclCanAssociateResources,
        'remove'       : aclCanAssociateResources,
        'register'     : registerUser,
        'requestEmail' : fetchUser,
        'confirmation' : pass,
        'passwordReset': pass,
        'reActivation' : pass
    },

    // resources owned, with full access and shareable by individual users
    userOwned: {
        'create'  : aclAssociateToUser,
        'find'    : aclFilterReadables,
        'findOne' : aclCanViewResource,
        'update'  : aclCanUpdateResource,
        'destroy' : aclCanDeleteResource,
        'populate': aclCanViewResource,
        'add'     : aclCanAssociateResources,
        'remove'  : aclCanAssociateResources
    }

};

/***
 * Assignment of resource classes to Controllers
 */
var resourcePolicies = {

    UserController: resourceClass.user,

    ProfileController: resourceClass.userOwned,

    NetworkController: resourceClass.userOwned,

    AuthorController: resourceClass.generic, // TODO: remove these boilerplate relics

    BookController: resourceClass.generic // TODO: remove these boilerplate relics

};

/***
 * Assignment of policies for generic controllers that control non-resources
 */
var basePolicies = {

    // Block all actions by default except by admins
    '*'           : aclAdminOnly,

    // pass access to authentication endpoint
    AuthController: {
        '*'            : pass,
        'provider'     : aclAdminOnly,
        'authenticated': aclAdminOnly,
        'checkPassword': authenticated
    },

    DatabaseController: {
        '*': aclAdminOnly
    },

    EmailController: {
        '*': emailWebhookValidate
    }
};

/***
 * Export policies by combining generic and resource controller policies
 */
module.exports.policies = _.merge(basePolicies, resourcePolicies);
