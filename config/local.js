'use strict';

/**
 * Local environment settings
 *
 * While you're DEVELOPING your app, this config file should include
 * any settings specifically for your development computer (db passwords, etc.)
 *
 * When you're ready to deploy your app in PRODUCTION, you can always use this file
 * for configuration options specific to the server where the app will be deployed.
 * But environment variables are usually the best way to handle production settings.
 *
 * PLEASE NOTE:
 *      This file is included in your .gitignore, so if you're using git
 *      as a version control solution for your Sails app, keep in mind that
 *      this file won't be committed to your repository!
 *
 *      Good news is, that means you can specify configuration for your local
 *      machine in this file without inadvertently committing personal information
 *      (like database passwords) to the repo.  Plus, this prevents other members
 *      of your team from committing their local configuration changes on top of yours.
 *
 * For more information, check out:
 * http://links.sailsjs.org/docs/config/local
 */
module.exports = {
    hash            : {
        secret    : 'HK5O3#!1KQPMMSI-OPA',
        expiration: 24 * 3600 * 1000
    },
    email           : {
        provider : 'mailgun',
        mailgun  : {
            apiKey   : 'key-49uxse4dtl4oxz55v33rd6940a4eu8r1',
            domain   : 'sandbox83047.mailgun.org',
            customKey: 'v:custom'
        },
        mailchimp: {
        }
    },
    connections     : {
        mysqlslush: {
            adapter  : 'sails-mysql',
            host     : 'localhost',
            user     : 'walid',
            password : 'my1mysql',
            database : 'slush',
            charset  : 'utf8',
            collation: 'utf8_swedish_ci'
        }
    },
    acl             : {
        backend: 'mongodb',
        mongodb: {
            url     : '127.0.0.1',
            database: 'acl'
        },
        redis  : {
            url           : '127.0.0.1',
            port          : 6379,
            no_ready_check: true
        },
        memory : {
        }
    },

    apiServerUrl    : 'http://localhost:' + (process.env.API_PORT || 1337),
    staticServerUrl : 'http://localhost:' + (process.env.STATIC_PORT || 3001),
    environment     : process.env.NODE_ENV || 'custom',

    log             : {
        level   : 'info',
        filePath: 'logs/application.log'
    }
};
