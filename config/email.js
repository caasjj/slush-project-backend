'use strict';

module.exports.email = {

    provider     :'mailgun',
    bypass       :false,
    defaultSender:'do-not-reply',
    adminSender  :'admin',
    dropLimit    :3,
    maxAttempts  :3

};