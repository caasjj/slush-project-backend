module.exports.enums = {

    network:{
        types :['Acquaintance', 'Colleague', 'Employee', 'Vendor'],
        status:['Invite', 'Withdrawn', 'Active', 'Rejected', 'Blocked', 'Deleted']
    },

    profileEmail:{
        types:['personal', 'work', 'other']
    },

    emailLog:{
        types:['confirmation', 'passwordReset', 'other']
    },

    user:{
        qualificationTypes:['provider', 'employee', 'vendor', 'serviceProvider'],
        membershipTypes   :['premium', 'member', 'freemium'],
        hashType: ['confirmation', 'passwordReset', 'reActivation', 'used']
    }
};