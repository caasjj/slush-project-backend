'use strict';

var request = require('supertest');
var expect = require('chai').expect;
var login = require("./../../helpers/login");
var _ = require('lodash');
var utils = require('./utils/Controllers.utils');

describe('***** UserController Tests *****', function UserController() {
    var adminToken = '';
    var demoToken = '';
    var userToken = '';
    var adminId;
    var demoId;
    var userId;

    var badData = [
        {
            "problem": 'missingPassword',
            "status" : 500,
            data     : {
                "username"     : "testuser",
                "qualification": "provider",
                "email"        : "testuser@testuser.com",
                "confirmed"    : true
            }
        },
        {
            "problem": "invalidPassword",
            "status" : 400,
            "data"   : {
                "username"     : "testuser",
                "password"     : "short",
                "qualification": "provider",
                "email"        : "testuser@testuser.com",
                "confirmed"    : true
            }
        },
        {
            "problem": "missingQualification",
            "status" : 400,
            "data"   : {
                "username" : "testuser",
                "password" : "testtest",
                "email"    : "testuser@testuser.com",
                "confirmed": true
            }
        },
        {
            "problem": "duplicatePassword",
            "status" : 400,
            "data"   : {
                "username"     : "testuser",
                "password"     : "testtest",
                "qualification": "provider",
                "email"        : "demo@demo.com",
                "confirmed"    : true
            }
        }
    ];

    var userData = {
        "password"     : "testtest",
        "username"     : "testuser",
        "qualification": "provider",
        "email"        : "testuser@testuser.com",
        "confirmed"    : true
    };

    var userDuplicateUsernameData = {
        "password"     : "testtest",
        "username"     : "demo",
        "qualification": "provider",
        "email"        : "testuser@testuser.com",
        "confirmed"    : true
    };

    var userDuplicateEmailData = {
        "password"     : "testtest",
        "username"     : "testuser",
        "qualification": "provider",
        "email"        : "demo@demo.com",
        "confirmed"    : true
    };

    var userAdminFalseData = {
        "password"     : "testtest",
        "username"     : "testadminfalse",
        "qualification": "provider",
        "email"        : "adminfalse@adminfalse.com",
        "confirmed"    : true,
        "admin"        : false
    };

    var userAdminTrueData = {
        "password"     : "testtest",
        "username"     : "testadmintrue",
        "qualification": "provider",
        "email"        : "admintrue@admintrue.com",
        "confirmed"    : true,
        "admin"        : true
    };

    var sourceProfile = {
        "provider"   : "local",
        "providerId" : "providerGivenId",
        "displayName": "userSelectedName",
        "familyName" : "Lafamilia",
        "givenName"  : "Alfonso",
        "middleName" : "bigboy",
        "photoUrl"   : "/path/to/photo"
    };

    function cleanUpUsers(done) {
        utils.cleanUpModels([
            {
                name : 'user',
                where: {
                    'id': {'>': 3}
                }
            },
            {
                name : 'passport',
                where: {
                    'id': {'>': 3}
                }
            }
        ],
            done);
    };

    var cleanUpProfiles = function cleanUpProfiles(done) {
        utils.cleanUpModels([
            { name   : 'profile',
                where: {
                    'id': {'>': 3}
                }
            }
        ], done);
    };

    before(function beforeTestLoginUsers(done) {

        async.parallel([
            function (cb) {
                login.authenticate('admin', function callback(error, result) {
                    cb(error, result);
                });
            },
            function (cb) {
                login.authenticate('demo', function callback(error, result) {
                    cb(error, result);
                });
            },
            function (cb) {
                login.authenticate('user', function callback(error, result) {
                    cb(error, result);
                });
            }

        ], function (err, results) {
                adminToken = results[0].token;
                adminId = results[0].user.id;
                demoToken = results[1].token;
                demoId = results[1].user.id;
                userToken = results[2].token;
                userId = results[2].user.id;
                done(err);
            }
        );
    });

    after(cleanUpUsers);

    describe('--- Providing bad data', function CheckBadDate() {

        badData.forEach(function (badSample) {
            describe('with ' + badSample.problem, function () {

                it('should not create stray records', function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/register')
                        .set('Content-Type', 'application/json')
                        .send(badSample.data)
                        .expect(badSample.status)
                        .end(function (err) {
                            if (err) return done(err);
                            utils.checkDatabase(done);
                        });
                });
            });

        });

    });

    describe('--- Registering a user account', function CreateNewUser() {

        afterEach(cleanUpUsers);

        describe('without logging in', function createNewUser() {

            it('should create a non-admin, non-confirmed user by default', function it(done) {

                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .send(userData)
                    .expect(201)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.admin).to.equal(false);
                        expect(result.body.confirmed).to.equal(false);
                        done();
                    });
            });

            it('should NOT create an account with duplicate username', function it(done) {

                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .send(userDuplicateUsernameData)
                    .expect(400)
                    .end(done);
            });

            it('should NOT create an account with duplicate email', function it(done) {

                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .send(userDuplicateEmailData)
                    .expect(400)
                    .end(done);
            });

            it('should still create a non-admin user with admin: false', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .send(userAdminFalseData)
                    .expect(201)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.admin).to.equal(false);
                        done();
                    });
            });

            it('should still create a non-admin user with admin: true', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .send(userAdminTrueData)
                    .expect(201)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.admin).to.equal(false);
                        done();
                    });
            });

        });

        describe('logged in as admin', function loggedInAsAdmin() {

            it('should create a non-admin user by default', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .send(userData)
                    .expect(201)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.admin).to.equal(false);
                        done();
                    });
            });

            it('should create a non-admin user with admin: false', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .send(userAdminFalseData)
                    .expect(201)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.admin).to.equal(false);
                        done();
                    });
            });

            it('should create a admin user with admin:true', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/register')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .send(userAdminTrueData)
                    .expect(201)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.admin).to.equal(true);
                        done();
                    });
            });
        });

    });

    describe('--- Modifying user account', function ModifyUser() {
        var testUserId,
            testUserToken;

        // need to create account with admin so confirmed=true
        beforeEach(function beforeModifyUserAccounts(done) {
            request(sails.hooks.http.app)
                .post('/user/register')
                .set('Content-Type', 'application/json')
                .set('authorization', 'bearer ' + adminToken)
                .send(userData)
                .end(function (err, user) {
                    if (err) return done(err);
                    testUserId = user.body.id;
                    login.authenticate({
                        "identifier": userData.username,
                        "password"  : userData.password
                    }, function (err, result) {
                        if (err) return done(err);
                        if (!result) return (new Error('Cannot create user for beforeEach'));
                        testUserToken = result.token;
                        done(err);
                    })
                });
        });

        afterEach(cleanUpUsers);

        describe('when logged in as a user', function loggedInAsUser() {

            it('should be able to modify own username', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/' + testUserId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + testUserToken)
                    .send({ 'username': 'joeblow'})
                    .expect(200)
                    .end(function (err, result) {
                        expect(result.body.username).to.equal('joeblow');
                        done(err);
                    });
            });

            it.skip('should be able to modify own password', function it(done) {
                request(sails.hooks.http.app)
                    .put('/user/' + userId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send({ 'password': 'newpassword'})
                    .expect(200)
                    .end(function (err) {
                        if (err) return done(err);
                        login.authenticate({
                            "identifier": userData.username,
                            "password"  : "newpassword"
                        }, function (err) {
                            done(err);
                        })
                    });
            });

            it('should be able to modify own email', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/' + testUserId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + testUserToken)
                    .send({ 'email': 'junk@junk.com'})
                    .expect(200)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.email).to.equal('junk@junk.com');
                        done(err);
                    });
            });

            it('should not be able to modify another user account', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/' + demoId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send({ 'email': 'junk@junk.com'})
                    .expect(403)
                    .end(done);
            });

        });

        describe('when logged in as admin', function loggedInAsAdmin() {

            it('should be able to modify any user account', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/' + testUserId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .send({ 'username': 'joeblow'})
                    .expect(200)
                    .end(function (err, result) {
                        expect(result.body.username).to.equal('joeblow');
                        done(err);
                    });
            })
        })

    });

    describe('--- Listing user accounts', function ListUsers() {

        describe('of all users', function ListAllUsers() {
            describe('when logged in as regular user', function loggedInAsUser() {
                it('should return user account', function it(done) {
                    request(sails.hooks.http.app)
                        .get('/user')
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .expect(200)
                        .end(function (err, result) {
                            expect(result.body.length).to.equal(1);
                            expect(result.body[0].id).to.equal(userId);
                            done();
                        });
                });
            });

            describe('when logged in as admin', function loggedInAsAdmin() {
                it('should list all users', function it(done) {
                    request(sails.hooks.http.app)
                        .get('/user')
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + adminToken)
                        .expect(200)
                        .end(function (err, result) {
                            expect(result.body.length).to.equal(3);
                            done();
                        });
                });

            });
        });

        describe('of own user account', function listOwnUserAccount() {
            it('should return the user account', function it(done) {
                request(sails.hooks.http.app)
                    .get('/user/' + userId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .expect(200)
                    .end(function (err, result) {
                        expect(result.body.id).to.equal(userId);
                        done();
                    });
            });
        });

        describe('of other user account', function listOwnUserAccount() {
            it('should not authorize', function it(done) {
                request(sails.hooks.http.app)
                    .get('/user/' + adminId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .expect(403)
                    .end(done);
            });
        });

    });

    describe('--- Deleting user account', function DeleteUser() {

        var testUserId,
            testUserToken;

        beforeEach(function beforeModifyUserAccounts(done) {
            request(sails.hooks.http.app)
                .post('/user/register')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'bearer ' + adminToken)
                .send(userData)
                .end(function (err, user) {
                    if (err) return done(err);
                    testUserId = user.body.id;
                    login.authenticate({
                        "identifier": userData.username,
                        "password"  : userData.password
                    }, function (err, result) {
                        testUserToken = result.token;
                        done(err);
                    })
                });
        });

        afterEach(cleanUpUsers);


        describe('when logged in as user', function () {

            describe('of own user', function () {

                it('should not authorize', function it(done) {
                    request(sails.hooks.http.app)
                        .delete('/user/' + testUserId)
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + testUserToken)
                        .send({})
                        .expect(200)
                        .end(function (err, result) {
                            expect(result.body).to.be.an('object');
                            expect(result.body.id).to.equal(testUserId);
                            done(err);
                        });
                });

            });

            describe('of other user', function () {

                it('should not authorize', function it(done) {
                    request(sails.hooks.http.app)
                        .delete('/user/' + demoId)
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + testUserToken)
                        .send({})
                        .expect(403)
                        .end(done);
                });
            });
        });

        describe('when logged in as admin', function () {

            it('should authorize', function it(done) {
                request(sails.hooks.http.app)
                    .delete('/user/' + testUserId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .send({})
                    .expect(200)
                    .end(function (err, result) {
                        expect(result.body).to.be.an('object');
                        expect(result.body.id).to.equal(testUserId);
                        done(err);
                    });
            });
        });

    });

    describe('--- Confirming a new User Account', function ConfirmAccount() {

        var testUser;
        var start = Date.now();


        before(function (done) {

            request(sails.hooks.http.app)
                .post('/user/register')
                .set('Content-Type', 'application/json')
                .send(userData)
                .expect(201)
                .end(function (err, result) {
                    testUser = result.body;
                    done();
                });

        });

        after(cleanUpUsers);


        describe('A un-confirmed user account', function NotConfirmed() {

            it('should not be confirmed', function it(done) {
                if (!testUser.hasOwnProperty('confirmed') ||
                    testUser.confirmed) {
                    return done(new Error('Confirmed'));
                }
                done();
            });

            it('should not be able to login without being confirmed', function it(done) {
                request(sails.hooks.http.app)
                    .post('/login')
                    .set('Content-Type', 'application/json')
                    .send({
                        "identifier": "testconfirmedfalse",
                        "password"  : "testtest"
                    })
                    .expect(401)
                    .end(done);
            });

            it('should not be able to request a confirmation email with missing username or password',
                function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/requestEmail')
                        .set('Content-Type', 'application/json')
                        .send({
                            "hashType": "confirmation"
                        })
                        .expect(400)
                        .end(done);
                });

            it('should not be able to request a confirmation email with invalid username or password',
                function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/requestEmail')
                        .set('Content-Type', 'application/json')
                        .send({
                            "identifier": "foo",
                            "password"  : "testtest",
                            "hashType"  : "confirmation"
                        })
                        .expect(400)
                        .end(done);
                });

            it('should be able to request a confirmation email with valid username and password',
                function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/requestEmail')
                        .set('Content-Type', 'application/json')
                        .send({
                            "identifier": "testuser",
                            "password"  : "testtest",
                            "hashType"  : "confirmation"
                        })
                        .expect(200)
                        .end(done);
                });

        });

        describe('After getting confirmation email ', function () {

            var hash;
            var hashType;

            it('should have a valid confirmation hash stored', function it(done) {
                request(sails.hooks.http.app)
                    .get('/user/' + testUser.id)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .expect(200)
                    .end(function (err, result) {
                        hash = result.res.body.hash;
                        hashType = result.res.body.hashType;
                        expect(result.res.body.hash).to.be.a('string');
                        expect(result.res.body.hashType).to.equal('confirmation');
                        expect(result.res.body.hashExpiration).to.be.below(Date.now());
                        expect(result.res.body.hashExpiration).to.be.above(start);
                        done();
                    });
            });

            it('should reject attempt to confirm with missing confirmation hash', function it(done) {
                request(sails.hooks.http.app)
                    .get('/user/confirmation')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .expect(400)
                    .end(done);
            });

            it('should reject attempt to confirm with invalid confirmation hash', function it(done) {
                request(sails.hooks.http.app)
                    .get('/user/confirmation?h=', 'SomeRandomStupidString')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + adminToken)
                    .expect(400)
                    .end(done);
            });

            it('should be able to confirm account with confirmation code', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/confirmation?h=' + hash)
                    .set('Content-Type', 'application/json')
                    .send({})
                    .expect(200)
                    .end(function (err) {
                        done(err);
                    });
            });

            it('should not be able to re-use a confirmation code a second time', function it(done) {
                request(sails.hooks.http.app)
                    .post('/user/confirmation?h=' + hash)
                    .set('Content-Type', 'application/json')
                    .send({})
                    .expect(400)
                    .end(function (err) {
                        done(err);
                    });
            });

            it('should not be able to request a confirmation email with valid username and password after account confirmed',
                function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/requestEmail')
                        .set('Content-Type', 'application/json')
                        .send({
                            "identifier": "testuser",
                            "password"  : "testtest",
                            "hashType"  : "confirmation"
                        })
                        .expect(400)
                        .end(function (err, result) {
                            if (err) return done(err);
                            expect(result.body.message).to.be.equal('User already confirmed');
                            done();
                        });
                });

            it('should be able to login after being confirmed', function it(done) {
                request(sails.hooks.http.app)
                    .post('/login')
                    .set('Content-Type', 'application/json')
                    .send({
                        "identifier": "testuser",
                        "password"  : "testtest"
                    })
                    .expect(200)
                    .end(function (err) {
                        done(err);
                    });
            });
        });

    });

    describe('--- Adding a user profile', function AddUserProfile() {

        describe('by associating a new profile ', function AssociateProfile() {

            afterEach(cleanUpProfiles);

            describe('without logging in', function associateNewProfile() {
                it('should not authenticate', function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/' + userId + '/profiles')
                        .set('Content-Type', 'application/json')
                        .send(sourceProfile)
                        .expect(401)
                        .end(done);
                });
            });

            describe('when logged in with valid token credential', function associateNewProfile() {
                it('should create user profile', function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/' + userId + '/profiles')
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .send(sourceProfile)
                        .expect(200)
                        .end(function (err, result) {
                            if (err) return done(err);
                            var profile = result.body.profiles[ result.body.profiles.length - 1 ];
                            expect(profile.createdUser).to.equal(userId);
                            expect(profile.updatedUser).to.equal(userId);
                            utils.checkModel(profile, sourceProfile, done);
                        });
                });

            });
        });

        describe('by associating an existing profile', function associateProfile() {

            var profileId;

            beforeEach(function (done) {
                request(sails.hooks.http.app)
                    .post('/profile')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send(sourceProfile)
                    .expect(200)
                    .end(function (err, result) {
                        if (err) {
                            return done(err);
                        }
                        expect(result.body.createdUser).to.equal(3);
                        profileId = result.body.id;
                        done();
                    });
            });

            afterEach(cleanUpProfiles);

            describe('to user\'s own account', function () {
                it('should leave profile associated with user', function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/' + userId + '/profiles/' + profileId)
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .send(sourceProfile)
                        .expect(200)
                        .end(function (err, result) {
                            if (err) return done(err);
                            var profile = result.body.profiles[ result.body.profiles.length - 1 ];
                            expect(profile.user).to.equal(userId);
                            done();
                        });
                });
            });

            describe('to a different user\'s account', function () {
                it('should not authorize', function it(done) {
                    request(sails.hooks.http.app)
                        .post('/user/' + demoId + '/profiles/' + profileId)
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .send(sourceProfile)
                        .expect(403)
                        .end(done);
                });
            });

        });

    });

    describe('--- Removing a user profile', function RemoveUserProfile() {

        var profileId;

        beforeEach(function (done) {
            request(sails.hooks.http.app)
                .post('/user/' + userId + '/profiles')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'bearer ' + userToken)
                .send(sourceProfile)
                .expect(200)
                .end(function (err, result) {
                    if (err) return done(err);
                    var user = result.body;
                    var profiles = user.profiles;
                    var newProfile = profiles[ profiles.length - 1];
                    expect(profiles.length).to.equal(2);
                    expect(newProfile.user).to.equal(userId);
                    profileId = newProfile.id;
                    done();
                });
        });

        afterEach(cleanUpProfiles);

        describe('by dissociating an associated profile', function DissociateUserProfile() {

            it('should remove the profile from user', function (done) {
                request(sails.hooks.http.app)
                    .delete('/user/' + userId + '/profiles/' + profileId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send({})
                    .expect(200)
                    .end(function (err, result) {
                        if (err) return done(err);
                        var user = result.body;
                        expect(user.id).to.equal(userId);
                        expect(user.profiles.length).to.equal(1);
                        expect(user.profiles[0].id).not.to.equal(profileId);
                        done();
                    });
            });
        });

        describe('by attempting to dissociate a profile that is not associated', function DissociateUserProfile() {

            it('should not authorize', function (done) {
                request(sails.hooks.http.app)
                    .delete('/user/' + userId + '/profiles/' + 1)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send({})
                    .expect(403)
                    .end(done);
            });
        });

        describe('by attempting to dissociate a profile that does not exist', function DissociateUserProfile() {

            it('should not affect the user account', function (done) {
                request(sails.hooks.http.app)
                    .delete('/user/' + userId + '/profiles/' + 9999)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send({})
                    .expect(403)
                    .end(done);
            });
        });

    });

    describe('--- Fetching a user profile', function FetchUserProfile() {
        var profileId;

        before(function (done) {
            request(sails.hooks.http.app)
                .post('/user/' + userId + '/profiles')
                .set('Content-Type', 'application/json')
                .set('Authorization', 'bearer ' + userToken)
                .send(sourceProfile)
                .expect(200)
                .end(function (err, result) {
                    if (err) return done(err);
                    var user = result.body;
                    var profiles = user.profiles;
                    var newProfile = profiles[ profiles.length - 1];
                    expect(profiles.length).to.equal(2);
                    expect(newProfile.user).to.equal(userId);
                    profileId = newProfile.id;
                    done();
                });
        });


        describe('Fetching a profile collection as a user', function FetchUserProfileCollection() {
            it('should return all profiles belonging to user', function (done) {
                request(sails.hooks.http.app)
                    .get('/user/' + userId + '/profiles')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send(sourceProfile)
                    .expect(200)
                    .end(function (err, result) {
                        if (err) return done(err);
                        var profiles = result.body;
                        expect(profiles.length).to.equal(2);
                        expect(profiles[0].user).to.equal(userId);
                        expect(profiles[1].user).to.equal(userId);
                        done();
                    });
            })
        })

        describe('Attempting to fetch a profile association', function FetchUserProfileCollection() {
            it('should return empty array', function (done) {
                request(sails.hooks.http.app)
                    .get('/user/' + userId + '/profiles/' + profileId)
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send(sourceProfile)
                    .expect(200)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body).to.be.instanceof(Array);
                        expect(result.body).to.be.empty;
                        done();
                    });
            })
        })

    })

})
;