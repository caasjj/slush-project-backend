var async = require('async');
var Promise = require('bluebird');

var parallel = async.parallel;

function cleanUpModels(models, done) {

    models = _.isArray(models) ? models : [models];

    parallel(models.map(function (model) {

        var modelName, where, sql;

        if (typeof model === 'string') {
            modelName = model;
            sql = 'DELETE FROM ' + modelName;
        }
        else {
            modelName = model.name;
            where = model.where;
            sql = 'DELETE FROM ' + modelName + ' WHERE ' + where;
        }

        return function (cb) {
            sails.models[modelName].query(sql, function (err) {
                cb(err)
            });
        }
    }), function (err) {
        done(err);
    });
}

function deleteModels(models, done) {

    models = _.isArray(models) ? models : [models];

    parallel(

        models.map(function (model) {

            var modelName, where;

            if (typeof model === 'string') {
                modelName = model;
            }
            else {
                modelName = model.name;
                where = model.where;
            }

            return function (cb) {
                sails.models[modelName].destroy(where).exec(cb);
            }

        }),

        done

    );

}

function checkModel(testModel, sourceModel, callback) {

    var mismatch = _.filter(_.keys(sourceModel), function (key) {
        return sourceModel[key] !== testModel[key];
    });

    mismatch = mismatch.length ? mismatch : null;
    callback(mismatch)
}

function checkDatabase(callback) {
    var models = sails.models;

    Promise.all([
            models.user.find(),
            models.passport.find(),
            models.profile.find()
        ])

        .then(function (results) {
            var users = results[0];
            var passports = results[1];
            var profiles = results[2];

            if (!users.length === 3 || !passports.length === 3 || !profiles.length === 3 ||
                !users[0].admin || users[1].admin || users[2].admin ||
                !users[0].username === 'admin' || !users[1].username === 'demo' || !users[2].username === 'user' ||
                !passports[0].user === 0 || !passports[1].user === 1 || !passports[2].user === 2 ||
                !profiles[0].user === 0 || !profiles[1].user === 1 || !profiles[2].user === 2
                ) {

                callback(new Error('Database modified'));
            }
            else {
                callback();
            }
        })

        .catch(function (err) {
            callback(err);
        });
}

module.exports = {
    cleanUpModels: deleteModels,
    checkModel   : checkModel,
    checkDatabase: checkDatabase
};