'use strict';

var request = require('supertest');
var expect = require('chai').expect;
var login = require("./../../helpers/login");
var _ = require('lodash');
var utils = require('./utils/Controllers.utils');

describe('***** ProfileController Tests *****', function ProfileController() {
    var
        adminToken,
        demoToken,
        userToken,
        adminId,
        demoId,
        userId;

    var sourceProfile = {
        "provider"   : "local",
        "providerId" : "providerGivenId",
        "displayName": "userSelectedName",
        "familyName" : "Lafamilia",
        "givenName"  : "Alfonso",
        "middleName" : "bigboy",
        "photoUrl"   : "/path/to/photo"
    };

    var badData = [
        {
            data   : {
                "providerId" : "providerGivenId1",
                "displayName": "userSelectedName1",
                "familyName" : "testFamilyName1",
                "givenName"  : "testGivenName1",
                "middleName" : "testMiddleName1",
                "photoUrl"   : "/path/to/photo"
            },
            problem: 'missing provider',
            status: 400
        },
        {
            data: {
                "provider"   : "local",
                "providerId" : "demoUser",
                "displayName": "userSelectedName2",
                "familyName" : "testFamilyName2",
                "givenName"  : "testGivenName2",
                "middleName" : "testMiddleName2",
                "photoUrl"   : "/path/to/photo"
            },
            problem: 'duplicateProviderId',
            status: 400
        }
    ];


    var cleanUpProfiles = function cleanUpProfiles(done) {
        utils.cleanUpModels([
            { name   : 'profile',
                where: {
                    'id': {'>': 3}
                }
            }
        ], done);
    };

    before(function beforeTestLoginUsers(done) {

        async.parallel([
            function (cb) {
                login.authenticate('admin', function callback(error, result) {
                    cb(error, result);
                });
            },
            function (cb) {
                login.authenticate('demo', function callback(error, result) {
                    cb(error, result);
                });
            },
            function (cb) {
                login.authenticate('user', function callback(error, result) {
                    cb(error, result);
                });
            }

        ], function (err, results) {
                adminToken = results[0].token;
                adminId = results[0].user.id;
                demoToken = results[1].token;
                demoId = results[1].user.id;
                userToken = results[2].token;
                userId = results[2].user.id;
                done(err);
            }
        );
    });

    describe('--- Providing bad data', function CheckBadData() {

        badData.forEach( function(badSample) {
            describe('with ' + badSample.problem, function() {
                it('should not create stray records', function it(done) {
                    request(sails.hooks.http.app)
                        .post('/profile')
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .send(badSample.data)
                        .expect(badSample.status)
                        .end(function (err) {
                            if (err) return done(err);
                            utils.checkDatabase(done);
                        });
                })
            })
        });

    });

    describe('--- Creating a profile', function CreateProfile() {

        afterEach(cleanUpProfiles);

        describe('without logging in', function notLoggedIn() {
            it('should not authenticate', function it(done) {
                request(sails.hooks.http.app)
                    .post('/profile')
                    .set('Content-Type', 'application/json')
                    .send(sourceProfile)
                    .expect(401)
                    .end(function (err) {
                        // this syntax may look silly but it's useful for setting breakpoints in IDE
                        done(err);
                    });
            });
        });

        describe('when logged in', function notLoggedIn() {
            it('should create an associated profile', function it(done) {
                request(sails.hooks.http.app)
                    .post('/profile')
                    .set('Content-Type', 'application/json')
                    .set('Authorization', 'bearer ' + userToken)
                    .send(sourceProfile)
                    .expect(200)
                    .end(function (err, result) {
                        if (err) return done(err);
                        expect(result.body.user).to.equal(userId);
                        expect(result.body.createdUser).to.equal(userId);
                        utils.checkModel(result.body, sourceProfile, done);
                    });
            });
        });

    });

    describe('--- Fetching profiles', function fetchProfiles() {

        describe('as a collection', function () {

            describe('logged in as user', function fetchUserProfiles() {

                it('should return user\'s own profiles', function (done) {
                    request(sails.hooks.http.app)
                        .get('/profile')
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .expect(200)
                        .end(function (err, result) {
                            if (err) return done(err);
                            expect(result.body[0].user).to.equal(userId);
                            done();
                        });
                });
            });

            describe('logged in as admin', function fetchUserProfiles() {

                it('should return all users\' profiles', function (done) {
                    request(sails.hooks.http.app)
                        .get('/profile')
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + adminToken)
                        .expect(200)
                        .end(function (err, result) {
                            if (err) return done(err);
                            expect(result.body.length).to.equal(3);
                            done();
                        });
                });
            });

        });

        describe('as individual profile', function () {

            describe('logged in as user', function fetchUserProfiles() {

                it('should return user\'s own profile', function (done) {
                    request(sails.hooks.http.app)
                        .get('/profile/' + userId)
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .expect(200)
                        .end(function (err, result) {
                            if (err) return done(err);
                            expect(result.body.user).to.equal(userId);
                            done();
                        });
                });

                it('should NOT authorize access to other users\' profile', function (done) {
                    request(sails.hooks.http.app)
                        .get('/profile/' + demoId)
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + userToken)
                        .expect(403)
                        .end(done);
                })
            });

            describe('logged in as admin', function fetchUserProfiles() {

                it('should return any user\'s profile', function (done) {

                    request(sails.hooks.http.app)
                        .get('/profile/' + userId)
                        .set('Content-Type', 'application/json')
                        .set('Authorization', 'bearer ' + adminToken)
                        .expect(200)
                        .end(function (err, result) {
                            if (err) return done(err);
                            expect(result.body.user).to.equal(userId);
                            done();
                        });
                });
            });

        });


    });

})
;