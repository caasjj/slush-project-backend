'use strict';

var expect = require('chai').expect;
var _ = require('lodash');
var async = require('async');


describe('***** ACL Service Tests *****', function AclService() {

    var ACL, profiles;

    before(function (done) {
        ACL = sails.services.acl;
        sails.models['profile'].find({where: 'id<4'}).populate('user').exec(function (err, result) {
            if (err) return done(err);
            profiles = result;
            done();
        })
    });

    describe('--- User access', function () {

        describe('to own user account', function () {

            it('should be allowed', function it(done) {

                async.parallel(profiles.map(function (profile) {

                    return function (cb) {
                        var userId = +profile.user.id;

                        ACL.isAllowed(userId, 'user', userId, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb( new Error('User ' + userId + ' does not have correct permission to own account'));
                        });
                    }

                }), done);

            });
        });

        describe('to another user account', function () {

            it('should be forbidden', function it(done) {

                async.parallel(profiles.map(function (profile) {

                    return function (cb) {
                        var user = profile.user,
                            userId = +profile.user.id,
                            otherUserId = userId - 1;

                        // check only non-admin users, we already know admins can do this
                        if (user.admin) return cb(false);
                        ACL.isAllowed(userId, 'user', otherUserId, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb(new Error( 'User ' + userId + ' can access account of ' + otherUserId) ) : cb();
                        });
                    }

                }), done);

            });
        });

        describe('to own profile', function () {

            it('should be allowed', function it(done) {

                async.parallel(profiles.map(function (profile) {

                    return function (cb) {

                        var userId = profile.user.id,
                            profileId = profile.id;

                        ACL.isAllowed(userId, 'profile', profileId, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb( new Error( 'User ' + userId + ' does not have correct permission to own profile.') );
                        });
                    }

                }), done);

            });
        });


        describe('to another user profile', function () {

            it('should be forbidden', function it(done) {

                async.parallel(profiles.map(function (profile) {

                    return function (cb) {
                        var user = profile.user,
                            userId = profile.user.id,
                            otherProfileId = profile.id - 1;

                        if (user.admin) return cb(false);
                        ACL.isAllowed(userId, 'profile', otherProfileId, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb( new Error('User ' + userId + ' has incorrect permission on profile ' + otherProfileId) ) : cb();
                        });
                    }

                }), done);

            });
        });

    });

    describe('--- Admin access', function () {

        describe('to any user account', function () {

            it('should be allowed', function it(done) {

                async.parallel(profiles.map(function (profile) {

                    return function (cb) {
                        ACL.isAllowed(1, 'user', profile.user.id, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb( new Error( 'User ' + profile.user.id + ' does not have correct permission') );
                        });
                    }

                }), done);

            });
        });


        describe('to any profile', function () {

            it('should be allowed', function it(done) {

                async.parallel(profiles.map(function (profile) {

                    return function (cb) {
                        var profileId = profile.id;
                        ACL.isAllowed(1, 'profile', profileId, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb( new Error( 'Admin does not have correct permissions on profile ' + profileId ));
                        });
                    }

                }), done);

            });
        });

    });

    describe('--- Networking', function () {

        describe('adding a user to own network', function () {

            it('should add the user to the network', function it(done) {

                var networkOwnerId = profiles[1].user.id,
                    networkMemberId = profiles[2].user.id;

                ACL.addUserToNetwork(networkOwnerId, networkMemberId, function (err) {
                    if (err) return done(err);
                    ACL.isInUserNetwork(networkOwnerId, networkMemberId, function (err, isMember) {
                        if (err) return done(err);
                        isMember ? done() : done( new Error('User ' + networkMemberId + ' is not a member of user ' + networkOwnerId + ' network'));
                    });
                })

            });
        });

        describe('removing an existing user from own network', function () {

            before(function (done) {
                var networkOwnerId = profiles[1].user.id,
                    networkMemberId = profiles[2].user.id;
                ACL.addUserToNetwork(networkOwnerId, networkMemberId, function (err) {
                    done(err);
                })
            });

            it('should remove the user from the network', function it(done) {
                var networkOwnerId = profiles[1].user.id,
                    networkMemberId = profiles[2].user.id;
                ACL.removeUserFromNetwork(networkOwnerId, networkMemberId, function (err) {
                    if (err) return done(err);
                    ACL.isInUserNetwork(networkOwnerId, networkMemberId, function (err, isMember) {
                        if (err) return done(err);
                        isMember ? done( new Error('User ' + networkMemberId + ' was not removed from network of user ' + networkOwnerId + ' network')) : done();
                    });
                })
            });
        })

    });

    describe('--- Network access', function () {



        describe('to a network shared profile', function () {

            before(function (done) {
                var networkOwnerId = profiles[1].user.id,
                    networkMemberId = profiles[2].user.id,
                    sharedProfile = profiles[1].id;
                ACL.addUserToNetwork(networkOwnerId, networkMemberId, function (err) {
                    if (err) return done(err);
                    ACL.giveNetworkAccessToResource(networkOwnerId, 'profile', sharedProfile, 'read', done);
                })
            });

            it('should be as per share permissions', function it(done) {

                var networkMemberId = profiles[2].user.id,
                    sharedProfile = profiles[1].id;

                async.parallel([
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'profile', sharedProfile, 'read', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb( new Error('does not have \'read\' permission'));
                        });
                    },
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'profile', sharedProfile, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb('has incorrect \'*\' permission') : cb();
                        });
                    },
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'profile', sharedProfile, 'write', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb( new Error('has incorrect \'write\' permission' )) : cb();
                        });
                    }
                ], done);


            });
        });

        describe('to a network shared arbitrary resource', function () {

            before(function (done) {
                var networkOwnerId = profiles[1].user.id,
                    networkMemberId = profiles[2].user.id,
                    resourceId = 1;

                ACL.addUserToNetwork(networkOwnerId, networkMemberId, function (err) {
                    if (err) return done(err);
                    ACL.giveNetworkAccessToResource(networkOwnerId, 'arbitraryResource', resourceId, ['read', 'write'], done);
                })
            });

            it('should be as per share permissions', function it(done) {

                var networkMemberId = profiles[2].user.id,
                    resourceId = 1;

                async.parallel([
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'arbitraryResource', resourceId, 'read', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb( new Error('does not have \'read\' permission') );
                        });
                    },
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'arbitraryResource', resourceId, 'write', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb(new Error('does not have \'write\' permission') );
                        });
                    },
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'arbitraryResource', resourceId, ['write','read'], function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb() : cb( new Error('does not have \'write\' permission') );
                        });
                    },
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'arbitraryResource', resourceId, ['write', 'read', 'update'], function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb(new Error('has incorrect \'*\' permission')) : cb();
                        });
                    },
                    function (cb) {
                        ACL.isAllowed(networkMemberId, 'arbitraryResource', resourceId, '*', function (err, allowed) {
                            if (err) return cb(err);
                            allowed ? cb(new Error('has incorrect \'*\' permission')) : cb();
                        });
                    }
                ], done);

            });
        });

    });

});
